//
//  InAppShop.m
//  Taxi Panik
//
//  Created by Dominik Horn on 13.03.13.
//
//

#import "InAppShop.h"
#import "Font.h"
#import "GameManager.h"

@implementation InAppShop
//@synthesize storeObjects = _storeObjects;

- (id)init
{
    self = [super init];
    if (self) {
        inAppStoreElements = [[NSMutableArray array] retain];
        [self getInAppList];
    }
    return self;
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    for (NSString* identifier in response.invalidProductIdentifiers) {
        NSLog(@"Could not retrieve Product for identifier: %@", identifier);
    }
    
//    self.storeObjects = response.products;
    [inAppStoreElements removeAllObjects];
    int xPos = 32;  /* 20 Px offset from the left side of the screen */
    for (SKProduct* product in response.products) {
        InAppShopElement* element = [InAppShopElement inAppElementWithRect:CGRectMake(xPos, 45, 256, h-(45 + 64)) product:product];
        element.delegate = self;
        [inAppStoreElements addObject:element];
//        NSLog(@"Added %@; %i | %i", product.productIdentifier, xPos, 20);
        xPos += 256 + 32;    /* One Element is 256 Px wide + 20 px offset */
    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Request %@ failed with Error: %@", request, error.localizedDescription);
}

- (void)getInAppList {
    SKProductsRequest* request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObjects:
                                                                                       WHEELS_25,
                                                                                        WHEELS_100,
                                                                                        WHEELS_1000,
                                                                                        nil]];
    request.delegate = self;
    [request start];
}

- (void)buyItem:(SKProduct*)item {
    if (![SKPaymentQueue canMakePayments]) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DISABLEDINAPP", nil)
                                                        message:NSLocalizedString(@"DISABLEDINAPPERROR", nil)
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"DISMISS", nil)
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    SKMutablePayment* payment = [SKMutablePayment paymentWithProduct:item];
    payment.quantity = 1;
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)checkBounds {
    /* Make sure we can't scroll too much */
    if (xScroll > 0) xScroll = 0;
    if (-xScroll > ((InAppShopElement*)[inAppStoreElements lastObject]).rect.origin.x - ((InAppShopElement*)[inAppStoreElements lastObject]).rect.size.width + 64) {
        xScroll = -(((InAppShopElement*)[inAppStoreElements lastObject]).rect.origin.x - ((InAppShopElement*)[inAppStoreElements lastObject]).rect.size.width + 64);
    }
//    if (xScroll < inAppStoreElements.count*(256 + 20) + 20) xScroll = inAppStoreElements.count*(256 + 20) + 20;
}

- (BOOL)receiveEvent:(int)event point:(CGPoint)p point2:(CGPoint)p2 {
    //Do stuff based on event
    if (event == EVENTTOUCHMOVE) {
        xScroll += p.x - p2.x;
        [self checkBounds];
    }
    p = CGPointMake(p.x - xScroll, p.y);
    p2 = CGPointMake(p2.x - xScroll, p2.y);

    for (InAppShopElement* element in inAppStoreElements) {
        if ([element recieveEvent:event p1:p p2:p2]) {
            return true;
        }
    }
    return false;
}

- (void)render {
    for (InAppShopElement* element in inAppStoreElements) {
        [element renderWithXOffset:xScroll];
    }
    if (inAppStoreElements.count == 0) {
        [[Font getInstance] drawString:@"Loading..." at:CGPointMake(w/2 - 50, h/2-10)];
    }
}

- (void)actionTriggered:(InAppShopElement*)element {
    [self buyItem:element.product];
}

- (void)dealloc
{
    [inAppStoreElements release];
    inAppStoreElements = nil;
    [super dealloc];
}

@end
