//
//  ScrollViewElement.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.11.12.
//
//

#import <UIKit/UIKit.h>
#import <OpenGLES/ES1/gl.h>

#define TEXTURE_NAME @"NoTexture.png"

@interface ScrollViewElement : NSObject {
    NSString* _name;
    
    CGRect _rect;
    BOOL _selected;
}

+ (ScrollViewElement*)scrollViewElementWithRect:(CGRect)rect name:(NSString*)string;
- (id)initScrollViewElementWithRect:(CGRect)rect name:(NSString*)string;

- (void)renderWithYOffset:(float)yOff;

@property (nonatomic, copy) NSString* name;
@property CGRect rect;
@property BOOL selected;

@end
