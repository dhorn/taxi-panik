//
//  Customer.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sprite.h"

@interface Customer : Sprite {
    int deathTimer;
    int animationNumber;
    
    BOOL angry;
    int angryAnimationNumber;
    float angryAnimationTimer;
    
    float frameRateIndependentTimer;
}
@property BOOL angry;

@end
