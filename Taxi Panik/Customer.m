//
//  Customer.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Customer.h"
#import "Sprite.h"
#import "GameManager.h"

@implementation Customer
@synthesize angry = angry;

- (id)initWithTexture:(NSString*)texture
                 rect:(CGRect)r
                speed:(Vector)s
                   ai:(int)ai
                angle:(float)a {
    
    if (self = [super initWithTexture:texture rect:r speed:s ai:ai angle:a]) {
        deathTimer = 500 + ((arc4random() % 16) * 30);    //Minimum: 16.6 seconds, maximum 30 seconds
        animationNumber = 0;
        frameRateIndependentTimer = 0;
        angry = false;
        angryAnimationNumber = 0;
    }
    return self;
}

- (void)update {
    [super update];
    if (!angry) {
        deathTimer--;
        if (deathTimer <= 0 && speed.x*speed.x <= 0.1 && speed.y*speed.y <= 0.1) {
            angry = true;
            self.textureName = @"AngryCustomer.png";
            texCoords = CGRectMake(0, 0, 0.25, 0.25);
        }
        if (!angry) {
        texCounter += deltatime;
            if (texCounter >= 1.0/8.0) {    //1/x x: How often do we want to change our frame
                texCounter -= 1.0/8.0;
                animationNumber ++;
                if (speed.y != 0 && speed.x != 0) {
                    if (animationNumber > 3) animationNumber = 0;
                    [self switchTexture];
                } else {
                    texCoords = CGRectMake(0, 0, 0.5, 0.5);
                }
            }
        }
    } else {
        if (angryAnimationNumber >= 8) { isActive = false; }
        angryAnimationTimer += deltatime;
        if (angryAnimationTimer >= 1.0/12.0) {   // How often do we want to update?
            angryAnimationTimer -= 1.0/12.0;
            angryAnimationNumber++;
            [self switchTexture];
        }
    }


    //get angle based on direction
    if (speed.y == 0) speed.y = 0.00000001;
    angle = getGrad(atan(speed.x/(-1*speed.y)));
    if (speed.y < 0) angle -= 180;
}

- (void)switchTexture {
    //TODO: Make Code nicer (No switch statement => fancy Math (kinda Fancy))
    if (angry) {
        switch (angryAnimationNumber) {
            case 0:
                texCoords = CGRectMake(0, 0, 0.25, 0.25);
                break;
            case 1:
                texCoords = CGRectMake(0.25, 0, 0.5, 0.25);
                break;
            case 2:
                texCoords = CGRectMake(0.5, 0, 0.75, 0.25);
                break;
            case 3:
                texCoords = CGRectMake(0.75, 0, 1, 0.25);
                break;
            case 4:
                texCoords = CGRectMake(0, 0.25, 0.25, 0.5);
                break;
            case 5:
                texCoords = CGRectMake(0.25, 0.25, 0.5, 0.5);
                break;
            case 6:
                texCoords = CGRectMake(0.5, 0.25, 0.75, 0.5);
                break;
            case 7:
                texCoords = CGRectMake(0.75, 0.25, 1, 0.5);
                break;
            case 8:
                texCoords = CGRectMake(0, 0.5, 0.25, 0.75);
                break;
            default:
                break;
        }
    } else {
        switch (animationNumber) {
            case 0:
                texCoords = CGRectMake(0, 0, 0.5, 0.5);
                break;
            case 1:
                texCoords = CGRectMake(0.5, 0, 1, 0.5);
                break;
            case 2:
                texCoords = CGRectMake(0, 0.5, 0.5, 1);
                break;
            case 3:
                texCoords = CGRectMake(0.5, 0.5, 1, 1);
                break;
            default:
                NSLog(@"Wow, lol?");
                break;
        }
    }
}

@end
