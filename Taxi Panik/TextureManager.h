//
//  TextureManager.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenGLES/ES1/gl.h>
#import "Texture.h"

@interface TextureManager : NSObject {
    NSMutableDictionary* dictionary;
    NSMutableDictionary* textureDependencies;
}

//class methods
+ (TextureManager*)getInstance;
- (void)preloader;

//intern
- (void)loadDependencies:(NSString*)texPack;
- (GLuint)getTextureID:(NSString*)name;
- (NSMutableDictionary*)getDictionary;
- (Texture*)getTextureObject:(NSString*)name;
- (GLubyte*)generatePixelDataFromImage:(UIImage*)pic texture:(Texture*)tex;
- (void)generateTexture:(GLubyte*)pixelData texture:(Texture*)tex;


//- (GLubyte*)generatePixelDataFromString:(NSString*)string;
//
//- (void)drawString:(NSString*)string withRect:(CGRect)rect;

@end
