//
//  CollisionBitmap.m
//  Taxi Panik
//
//  Created by Dominik Horn on 09.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CollisionBitmap.h"
#import "Bit.h"

@implementation CollisionBitmap
@synthesize bits, size;

- (id)initWithSize:(CGSize)s {
    if (self = [super init]) {
        self.size = s;
        self.bits = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.bits = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)addBit:(Bit*)bit {
    [self.bits setObject:bit forKey:[NSString stringWithFormat:@"%f\n%f", bit.position.x, bit.position.y]];
}

- (Bit*)getBitAt:(CGPoint)pos {
    return (Bit*)[self.bits objectForKey:[NSString stringWithFormat:@"%f\n%f", pos.x, pos.y]];
}

- (NSArray*)getBitsForSpace:(CGRect)space {
    NSMutableArray* objects = [NSMutableArray arrayWithCapacity:space.size.width*space.size.height];
    for (Bit* bit in [self.bits allValues]) {
        if (bit.position.x >= space.origin.x && bit.position.x <= space.origin.x+space.size.width
          &&bit.position.y >= space.origin.y && bit.position.y <= space.origin.y+space.size.height) {
            [objects addObject:[bit copy]];
        }
    }
    return [objects copy];
}

@end
