//
//  Button.h
//  Taxi Panik
//
//  Created by Dominik Horn on 13.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuElement.h"

@class GameManager;

@interface Button : MenuElement {
    BOOL selected;
}

@end
