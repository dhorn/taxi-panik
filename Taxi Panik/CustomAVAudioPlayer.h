//
//  CustomAVAudioPlayer.h
//  Taxi Panik
//
//  Created by Dominik Horn on 29.10.12.
//
//

#import <AVFoundation/AVFoundation.h>

@interface CustomAVAudioPlayer : AVAudioPlayer {
    NSString* _name;
    BOOL _wasPlaying;
}

@property (copy, nonatomic) NSString* name;
@property BOOL wasPlaying;

@end
