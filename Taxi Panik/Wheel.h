//
//  Wheel.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.13.
//
//

#import "Sprite.h"

@interface Wheel : Sprite {
    int deathTimer;
}

@property BOOL clicked;
@property Vector direction;

@end
