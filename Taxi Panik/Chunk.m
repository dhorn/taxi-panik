//
//  Chunk.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Chunk.h"
#import "GameManager.h"
#import "Player.h"

@implementation Chunk
//@synthesize pos;
@synthesize neighboors;
@synthesize canspawnCar;

- (id)init
{
    self = [super init];
    if (self) {
        textureName = @"NoTexture.png"; 
    }
    return self;
}

- (void)render {
    GLshort vertices [] = {
        0.0, self.rect.size.height,
        self.rect.size.width, self.rect.size.height,
        0.0, 0.0,
        self.rect.size.width, 0.0,
    };
    
    GLfloat textureCoords[ ] = {                 
        texCoords.origin.x,    texCoords.size.height, //links unten
        texCoords.size.width,  texCoords.size.height, //rechts unten
        texCoords.origin.x,    texCoords.origin.y, //links oben
        texCoords.size.width,  texCoords.origin.y  //rechts oben                      
    };

    glColor4f(1, 1, 1, 1);
    glBindTexture(GL_TEXTURE_2D, [[TextureManager getInstance] getTextureID:textureName]);
    glVertexPointer(2, GL_SHORT, 0, &vertices);
    glTexCoordPointer(2, GL_FLOAT, 0, textureCoords);
    
    glPushMatrix();
    glTranslatef(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/2, 0.0);
    glRotatef(angle, 0, 0, 1); //angle = 0 = keine Rotation
    glTranslatef(-rect.size.width/2, -rect.size.height/2, 0.0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

#pragma mark -
#pragma mark Getter, Setter
- (void)setType:(int)t {    
    type = t;
    int ran = arc4random() % 4;
    int ranHouseX = arc4random() % 2;
    int ranHouseY = arc4random() % 2;
    switch (type) {
        case STREET_STRAIGHT:
            textureName = @"Street.png";
            texCoords = CGRectMake(ran/4,  0.0, ran/4+0.25, 0.25);
            break;
        case STREET_TURN:
            textureName = @"Street.png";
            texCoords = CGRectMake(ran/4, 0.25, ran/4+0.25,  0.5);
            break;
        case STREET_3WAYCROSS:
            textureName = @"Street.png";
            texCoords = CGRectMake(ran/4,  0.5, ran/4+0.25, 0.75);
            break;
        case STREET_4WAYCROSS:
            textureName = @"Street.png";
            texCoords = CGRectMake(ran/4, 0.75, ran/4+0.25,  1.0);
            break;
        case DECORATION_GRASS:
            textureName = @"grass.png";
            texCoords = CGRectMake(0.0, 0.0, 1.0, 1.0);
            break;
        case DECORATION_HOUSE:
            textureName = @"House.png";
            texCoords = CGRectMake(0, 0, 0.5, 0.5);
            //texCoords = CGRectMake(ranHouseX/2.0, ranHouseY/2.0, ranHouseX/2.0+0.5, ranHouseY/2.0+0.5);
            break;
        case DECORATION_SNOW:
            textureName = @"Snow.png";
            texCoords = CGRectMake(0, 0, 1, 1);
            break;
        default:
            break;
    }
}

- (int)type {
    return type;
}

@end
