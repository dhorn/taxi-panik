//
//  Math.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Math.h"
#import "Sprite.h"
#import "GameObject.h"

float dotProduct(Vector vec1, Vector vec2) {
    vec1 = normalize(vec1);
    vec2 = normalize(vec2);
    
    /* Invert since our coord system is inverted */
    vec1.y*=-1;
    vec2.y*=-1;
    
    return vec1.x*vec2.x+vec1.y*vec2.y;
}

/* works fine till 180 degrees :( */
float angleBetween(Vector vec1, Vector vec2) {
    return getGrad(acosf(dotProduct(vec1, vec2)));
}

float angle2D(Vector direction, Vector face) {
    return getGrad(atan2f(direction.x, -1*direction.y))-getGrad(atan2f(face.x, -1*face.y)); /* Invert y because our coordinate system is turned around */
}

inline CollisionBox makeCollisionBox(CGRect rect) {
    CGPoint topLeft = rect.origin;
    CGPoint downRight = CGPointMake(rect.origin.x+rect.size.width, rect.origin.y+rect.size.height);
    CollisionBox c; c.topLeft = topLeft; c.downRight = downRight;
    c.topRight = CGPointMake((downRight.x-topLeft.x)+topLeft.x, topLeft.y);
    c.downLeft = CGPointMake(topLeft.x, (downRight.y-topLeft.y)+topLeft.y);
    c.max = topLeft;
    c.min = topLeft;
    if (c.max.x < c.topRight.x)
        c.max.x = c.topRight.x;
    if (c.max.y < c.topRight.y)
        c.max.y = c.topRight.y;
    if (c.min.x > c.topRight.x)
        c.min.x = c.topRight.x;
    if (c.min.y > c.topRight.y)
        c.min.y = c.topRight.y;
    
    if (c.max.x < c.downRight.x)
        c.max.x = c.downRight.x;
    if (c.max.y < c.downRight.y)
        c.max.y = c.downRight.y;
    if (c.min.x > c.downRight.x)
        c.min.x = c.downRight.x;
    if (c.min.y > c.downRight.y)
        c.min.y = c.downRight.y;
    
    if (c.max.x < c.downLeft.x)
        c.max.x = c.downLeft.x;
    if (c.max.y < c.downLeft.y)
        c.max.y = c.downLeft.y;
    if (c.min.x > c.downLeft.x)
        c.min.x = c.downLeft.x;
    if (c.min.y > c.downLeft.y)
        c.min.y = c.downLeft.y;
    return c;
}

inline CollisionBox calcOOCollisionBox(CollisionBox box, float angle) {
    float width    = box.topRight.x - box.topLeft.x;
    float height   = box.downLeft.y - box.topLeft.y;
    box.angle      = angle;
    float rad      = getRad(box.angle);
    float cnstTan  = atanf(width/height);
    
    box.topLeft   = CGPointMake(sin(rad-cnstTan)*cnstSqrt, -cos(rad-cnstTan)*cnstSqrt);
    box.topRight  = CGPointMake(sin(rad+cnstTan)*cnstSqrt, -cos(rad+cnstTan)*cnstSqrt);
    box.downRight = CGPointMake(-box.topLeft.x,  -box.topLeft.y);
    box.downLeft  = CGPointMake(-box.topRight.x, -box.topRight.y);
    box.min = box.topLeft;
    box.max = box.topLeft;
    if (box.max.x < box.topRight.x)
        box.max.x = box.topRight.x;
    if (box.max.y < box.topRight.y)
        box.max.y = box.topRight.y;
    if (box.min.x > box.topRight.x)
        box.min.x = box.topRight.x;
    if (box.min.y > box.topRight.y)
        box.min.y = box.topRight.y;
    
    if (box.max.x < box.downRight.x)
        box.max.x = box.downRight.x;
    if (box.max.y < box.downRight.y)
        box.max.y = box.downRight.y;
    if (box.min.x > box.downRight.x)
        box.min.x = box.downRight.x;
    if (box.min.y > box.downRight.y)
        box.min.y = box.downRight.y;
    
    if (box.max.x < box.downLeft.x)
        box.max.x = box.downLeft.x;
    if (box.max.y < box.downLeft.y)
        box.max.y = box.downLeft.y;
    if (box.min.x > box.downLeft.x)
        box.min.x = box.downLeft.x;
    if (box.min.y > box.downLeft.y)
        box.min.y = box.downLeft.y;
    return box;
}

inline float getRad(float grad) {
    return (M_PI/180)*grad;
}

inline float getGrad(float rad) {
    return rad/(M_PI/180);
}

inline Line makeLine(float m, float t) {
    Line line; line.m = m; line.t = t; return line;
}

inline Vector makeVector(float x, float y, float z) {
    Vector v; v.x = x; v.y = y; v.z = z; return v;
}

inline float squareDistCCOfRects(CGRect rect1, CGRect rect2) {
    CGPoint center1 = CGPointMake(rect1.size.width/2+rect1.origin.x, rect1.size.height+rect1.origin.y); 
    CGPoint center2 = CGPointMake(rect2.size.width/2+rect2.origin.x, rect2.size.height+rect2.origin.y);
    return ((center1.x-center2.x)*(center1.x-center2.x)+(center1.y-center2.y)*(center1.y-center2.y));
}

Vector normalize(Vector v) {
    double length = sqrt(v.x*v.x+v.y*v.y+v.z*v.z);
    return makeVector(v.x/length, v.y/length, v.z/length);
}

inline BOOL pointRectCol(CGRect r, CGPoint p) {
    if (p.x >= r.origin.x && p.x <= r.origin.x+r.size.width) {
        if (p.y >= r.origin.y && p.y <= r.origin.y+r.size.height) {
            return true;
        }
    }
    return false;
}

inline BOOL rectRectCol(CGRect rect1, CGRect rect2) {
    //Rect 1
    int x1=rect1.origin.x;//Rect1: Punkt links oben
    int y1=rect1.origin.y;
    int w1=rect1.size.width;
    int h1=rect1.size.height;
    
    //Rect 2
    int x3=rect2.origin.x; //Rect2: Punkt links oben
    int y3=rect2.origin.y;
    int w2=rect2.size.width;
    int h2=rect2.size.height;
    
	int x2=x1+w1, y2=y1+h1;	//Rect1: Punkt rechts unten
	int x4=x3+w2, y4=y3+h2;	//Rect2: Punkt rechts unten
	
    if (   x2 >= x3
        && x4 >= x1
        && y2 >= y3
        && y4 >= y1) {
        return true;
    }
    return false;
}

inline BOOL sphereCol(GameObject* obj1, GameObject* obj2) {
    return [obj1 getRadius]*[obj1 getRadius]+[obj2 getRadius]*[obj2 getRadius] >= squareDistCCOfRects(obj1.rect, obj2.rect) ? true : false;
}

inline BOOL aabbCol(GameObject* obj1, GameObject* obj2) {
    CGRect rect1 = obj1.rect;
    CGRect rect2 = obj2.rect;
    
    //Rect 1
    int x1=rect1.origin.x;//Rect1: Punkt links oben
    int y1=rect1.origin.y;
    int w1=rect1.size.width;
    int h1=rect1.size.height;
    
    //Rect 2
    int x3=rect2.origin.x; //Rect2: Punkt links oben
    int y3=rect2.origin.y;
    int w2=rect2.size.width;
    int h2=rect2.size.height;
    
	int x2=x1+w1, y2=y1+h1;	//Rect1: Punkt rechts unten
	int x4=x3+w2, y4=y3+h2;	//Rect2: Punkt rechts unten 
	
    if (   x2 >= x3
        && x4 >= x1
        && y2 >= y3
        && y4 >= y1) {
        return true;   		   	 
    }
    return false;
}
inline BOOL oobbCol(GameObject* obj1, GameObject* obj2) {
    CollisionBox box1 = calcOOCollisionBox(makeCollisionBox(obj1.rect), obj1.angle);
    CollisionBox box2 = calcOOCollisionBox(makeCollisionBox(obj2.rect), obj2.angle);
    if (box1.max.x < box2.min.x)
        return false;
    if (box1.max.y < box2.min.y)
        return false;
    if (box2.max.x < box1.min.x)
        return false;
    if (box2.max.y < box1.max.y)
        return false;
    return true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@implementation Math

+ (Math*)instance {
    static Math* math;
    if (!math) {
        math = [[Math alloc] init];
    }
    return math;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

//- (BOOL)colOOBBSprite:(Sprite*)sprite1 sprite:(Sprite*)sprite2 {
//    //BS check to see if collision is even possible, then heavy(er) OOBB check
//    float cnstSqrt1 = sqrtf(sprite1.rect.size.width/2*sprite1.rect.size.width/2+sprite1.rect.size.height/2*sprite1.rect.size.height/2);
//    float cnstSqrt2 = sqrtf(sprite2.rect.size.width/2*sprite2.rect.size.width/2+sprite2.rect.size.height/2*sprite2.rect.size.height/2);
//    float disToCol = cnstSqrt1+cnstSqrt2;
//    CGPoint pos1 = CGPointMake(sprite1.rect.origin.x+sprite1.rect.size.width/2, sprite1.rect.origin.y+sprite1.rect.origin.y);
//    CGPoint pos2 = CGPointMake(sprite2.rect.origin.x+sprite2.rect.size.width/2, sprite2.rect.origin.y+sprite2.rect.origin.y);
//    CGPoint vec = CGPointMake(pos1.x-pos2.x, pos1.y-pos2.y);
//    float dist = vec.x*vec.x+vec.y*vec.y;
//    if (dist >= disToCol*disToCol) {
//        NSLog(@"false");
//        return false;
//    }
//    NSLog(@"going for semi Heavy, survived lite calculations");
//    //semi Heavy Calculations
//    CollisionBox b1 = makeCollisionBox(sprite1.rect.origin, CGPointMake(sprite1.rect.origin.x+sprite1.rect.size.width,
//                                                                        sprite1.rect.origin.x+sprite1.rect.size.height));
//    CollisionBox b2 = makeCollisionBox(sprite2.rect.origin, CGPointMake(sprite2.rect.origin.x+sprite2.rect.size.width,
//                                                                        sprite2.rect.origin.x+sprite2.rect.size.height));
//    if (b1.max.x < b2.min.x)
//        return false;
//    if (b2.max.x < b1.min.x)
//        return false;
//    if (b1.max.y > b2.min.y)
//        return false;
//    if (b2.max.y > b1.min.y)
//        return false;
//    
//    NSLog(@"Going for heavy check now, survived others");
//    //Heavy Calculations (make lines of points and test every line
//    Line line1 [4] = {makeLine((o1.points[0].y-o1.points[1].y)/(o1.points[0].x-o1.points[1].x), o1.points[0].y),  //topleft-topright
//                      makeLine((o1.points[1].y-o1.points[2].y)/(o1.points[1].x-o1.points[2].x), o1.points[1].y),  //topright-downright
//                      makeLine((o1.points[2].y-o1.points[3].y)/(o1.points[2].x-o1.points[3].x), o1.points[2].y),  //downright-downleft
//                      makeLine((o1.points[3].y-o1.points[0].y)/(o1.points[3].x-o1.points[0].x), o1.points[3].y)}; //downleft-topleft
//    
//    Line line2 [4] = {makeLine((o2.points[0].y-o2.points[1].y)/(o2.points[0].x-o2.points[1].x), o2.points[0].y),  //topleft-topright
//                      makeLine((o2.points[1].y-o2.points[2].y)/(o2.points[1].x-o2.points[2].x), o2.points[1].y),  //topright-downright
//                      makeLine((o2.points[2].y-o2.points[3].y)/(o2.points[2].x-o2.points[3].x), o2.points[2].y),  //downright-downleft
//                      makeLine((o2.points[3].y-o2.points[0].y)/(o2.points[3].x-o2.points[0].x), o2.points[3].y)}; //downleft-topleft
//    
//    for (int cnt = 0; cnt < sizeof(line1) / sizeof(Line); cnt++) {
//        for (int cnt2 = 0; cnt2 < sizeof(line2) / sizeof(Line); cnt2++) {
//            Line opline1 = line1[cnt];
//            Line opline2 = line2[cnt2];
//            float x = (opline2.t/opline1.t)/(opline2.m/opline1.m);
//            if ((x*opline1.m+opline1.t) == (x*opline2.m+opline2.t)) {
//                NSLog(@"true");
//                return true;
//            }
//        }
//    }
//    NSLog(@"Couldn't prove collision wrong => true");
//    return true;
//}

@end
