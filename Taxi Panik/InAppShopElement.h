//
//  InAppShopElement.h
//  Taxi Panik
//
//  Created by Dominik Horn on 22.03.13.
//
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/gl.h>
#import <StoreKit/StoreKit.h>
#import "InAppStoreElementDelegate.h"

@interface InAppShopElement : NSObject {
    SKProduct* _product;
    
    CGRect _rect;
    BOOL _selected;
    
    id<InAppStoreElementDelegate> _delegate;
}

+ (InAppShopElement*)inAppElementWithRect:(CGRect)rect product:(SKProduct*)product;
- (id)initWithRect:(CGRect)rect product:(SKProduct*)product;

- (void)renderWithXOffset:(float)xOff;
- (BOOL)recieveEvent:(int)event p1:(CGPoint)p1 p2:(CGPoint)p2;

@property (nonatomic, assign) id<InAppStoreElementDelegate> delegate;
@property (nonatomic, retain) SKProduct* product;
@property CGRect rect;
@property BOOL selected;

@end