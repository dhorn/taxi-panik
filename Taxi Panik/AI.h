//
//  AI.h
//  Taxi Panik
//
//  Created by Dominik Horn on 11.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Sprite;

enum AITypes {
    AIDEFAULT,
    AICAR,
    AICUSTOMER
};

@interface AI : NSObject {
    
}

+ (AI*)getInstance;

- (void)aiTick:(Sprite*)object;

@end
