//
//  InAppShopElement.m
//  Taxi Panik
//
//  Created by Dominik Horn on 22.03.13.
//
//

#import "InAppShopElement.h"
#import "MenuElement.h"
#import "Font.h"

@implementation InAppShopElement
@synthesize product = _product, rect = _rect, selected = _selected, delegate = _delegate;

+ (InAppShopElement*)inAppElementWithRect:(CGRect)rect product:(SKProduct*)product {
    return [[[InAppShopElement alloc] initWithRect:rect product:product] autorelease];
}

- (id)initWithRect:(CGRect)rect product:(SKProduct*)product {
    self = [super init];
    if (self) {
        self.product = product;
        self.rect = rect;
    }
    return self;
}

- (void)renderWithXOffset:(float)xOff {
    GLshort vertices [] = {
        0.0, _rect.size.height,
        _rect.size.width, _rect.size.height,
        0.0, 0.0,
        _rect.size.width, 0.0,
    };
    GLfloat textureCoords[8] = {
        0, 1, //links unten
        0.5, 1, //rechts unten
        0, 0, //links oben
        0.5, 0  //rechts oben
    };
    
    if (self.selected) {
        textureCoords[0] = 0.5;
        textureCoords[2] = 1.0;
        textureCoords[4] = 0.5;
        textureCoords[6] = 1.0;
    }
    
    glBindTexture(GL_TEXTURE_2D, [[TextureManager getInstance] getTextureID:@"InAppStoreElement.png"]);
    glColor4f(1, 1, 1, 1);
    glVertexPointer(2, GL_SHORT, 0, vertices);
    glTexCoordPointer(2, GL_FLOAT, 0, textureCoords);
    
    glPushMatrix();
    glTranslatef(_rect.origin.x+xOff, _rect.origin.y, 0.0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();
    
    [[Font getInstance] drawString:self.product.localizedTitle
                                       at:CGPointMake(self.rect.origin.x + xOff + 10, self.rect.origin.y)];
    /* 25 chars per line */
    NSMutableArray* lines = [NSMutableArray arrayWithCapacity:10];
    NSString* currentString = @"";
    int lengthMax = 22;
    int length = 0;
    for (NSString* word in [self.product.localizedDescription componentsSeparatedByString:@" "]){
        length += word.length + 1;
        if (length < lengthMax) {
            if ([currentString isEqualToString:@""]) {
                currentString = word;
            } else {
                currentString = [NSString stringWithFormat:@"%@ %@", currentString, word];
            }
        } else {
            length = 0;
            [lines addObject:currentString];
            currentString = word;
        }
    }
    [lines addObject:currentString];
    currentString = @"";
    
    for (NSString* line in lines) {
        [[Font getInstance] drawString:line
                                    at:CGPointMake(self.rect.origin.x + xOff + 10, self.rect.origin.y + 32*([lines indexOfObject:line]+1))];
    }
    [[Font getInstance] drawString:[NSString stringWithFormat:@"%@", self.product.price]
                                    at:CGPointMake(self.rect.origin.x + xOff + 10, self.rect.origin.y - 32 + self.rect.size.height)];
}

- (BOOL)recieveEvent:(int)event p1:(CGPoint)p1 p2:(CGPoint)p2 {
    switch (event) {
        case EVENTTOUCHDOWN:
            if ((p1.x >= self.rect.origin.x && p1.x <= self.rect.origin.x + self.rect.size.width) &&
                (p1.y >= self.rect.origin.y && p1.y <= self.rect.origin.y + self.rect.size.height)) {
                    self.selected = true;
            }
            break;
        case EVENTTOUCHMOVE:
            self.selected = false;
            break;
        case EVENTTOUCHUP:
            if (self.selected && self.delegate) [self.delegate actionTriggered:self];
            if ((p1.x >= self.rect.origin.x && p1.x <= self.rect.origin.x + self.rect.size.width) &&
                (p1.y >= self.rect.origin.y && p1.y <= self.rect.origin.y + self.rect.size.height)) {
                self.selected = false;
            }
            break;
        case EVENTTOUCHCANCELED:
            if ((p1.x >= self.rect.origin.x && p1.x <= self.rect.origin.x + self.rect.size.width) &&
                (p1.y >= self.rect.origin.y && p1.y <= self.rect.origin.y + self.rect.size.height)) {
                self.selected = false;
            }
            break;
        default:
            break;
    }
    return false;
}

@end
