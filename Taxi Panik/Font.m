//
//  Font.m
//  Taxi Panik
//
//  Created by Dominik Horn on 28.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Font.h"

@implementation Font
@synthesize fontTextures;

+ (Font*)getInstance {
    static Font* f;
    if (!f) {
        f = [[Font alloc] init];
    }
    return f;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.fontTextures = [[[NSMutableDictionary alloc] initWithCapacity:95] retain];
        [self createStringArray];
    }
    return self;
}

- (void)drawString:(NSString*)string at:(CGPoint)p {
    //Split string in array of single chars
    const char* singleChars = [string cStringUsingEncoding:NSUTF8StringEncoding];
    CGPoint refPoint = p;
    for (int i = 0; i < [string length]; i++) {
        char object = singleChars[i];
        unsigned int key = object;
//        printf("String: %c\nNumber: %i\n\n", object, key);
        float offset = WIDTH/3;
        if (key ==  32) { offset/=2; refPoint.x+=offset; continue; }
        
        GLshort vertices [] = {
            0, HEIGHT-HEIGHT/3,          //links unten
            WIDTH-WIDTH/3, HEIGHT-HEIGHT/3,      //rechts unten
            0, 0,               //links oben
            WIDTH-WIDTH/3, 0            //rechts oben
        };
        GLshort texCoords [] = {
            0, 1,   //links unten
            1, 1,   //rechts unten
            0, 0,   //links oben
            1, 0    //rechts oben
        };
        //glEnable(GL_TEXTURE_2D);
        NSNumber* num = [self.fontTextures objectForKey:[NSNumber numberWithUnsignedInt:key]];
        glBindTexture(GL_TEXTURE_2D, [num unsignedIntValue]);
        glVertexPointer(2, GL_SHORT, 0, vertices);
        glTexCoordPointer(2, GL_SHORT, 0, texCoords);    
        
        glPushMatrix();
        glTranslatef(refPoint.x, refPoint.y, 0);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glPopMatrix();
        
        //glDisable(GL_TEXTURE_2D);
//        if (singleChars[i+1] == 105) { offset*=1.2; }
//        if (singleChars[i+1] == 33)  { offset*=0.8; }
//        if (key == 105 || key == 33 || key == 116 || key == 108) { offset/=2; }
//        if (key == 114) { offset = offset-offset/3; }
        refPoint.x+=offset;  //Advance more to the left
    }
}

- (void)drawColoredString:(NSString*)string at:(CGPoint)p color:(Vector)color {
    const char* singleChars = [string cStringUsingEncoding:NSUTF8StringEncoding];
    CGPoint refPoint = p;
    for (int i = 0; i < [string length]; i++) {
        char object = singleChars[i];
        unsigned int key = object;
        //        printf("String: %c\nNumber: %i\n\n", object, key);
        float offset = WIDTH/3;
        if (key ==  32) { offset/=2; refPoint.x+=offset; continue; }
        
        GLshort vertices [] = {
            0, HEIGHT-HEIGHT/3,          //links unten
            WIDTH-WIDTH/3, HEIGHT-HEIGHT/3,      //rechts unten
            0, 0,               //links oben
            WIDTH-WIDTH/3, 0            //rechts oben
        };
        GLshort texCoords [] = {
            0, 1,   //links unten
            1, 1,   //rechts unten
            0, 0,   //links oben
            1, 0    //rechts oben
        };
        //glEnable(GL_TEXTURE_2D);
        NSNumber* num = [self.fontTextures objectForKey:[NSNumber numberWithUnsignedInt:key]];
        glBindTexture(GL_TEXTURE_2D, [num unsignedIntValue]);
        glVertexPointer(2, GL_SHORT, 0, vertices);
        glTexCoordPointer(2, GL_SHORT, 0, texCoords);
        
        glPushMatrix();
        
        glTranslatef(refPoint.x, refPoint.y, 0);
        glColor4f(color.x/255, color.y/255, color.z/255, 1);
        glColorMask(true, true, true, false);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glColor4f(1, 1, 1, 1);
        glColorMask(true, true, true, true);
        
        glPopMatrix();
        
        //glDisable(GL_TEXTURE_2D);
        //        if (singleChars[i+1] == 105) { offset*=1.2; }
        //        if (singleChars[i+1] == 33)  { offset*=0.8; }
        //        if (key == 105 || key == 33 || key == 116 || key == 108) { offset/=2; }
        //        if (key == 114) { offset = offset-offset/3; }
        refPoint.x+=offset;  //Advance more to the left
    }
}

- (void)drawColoredString:(NSString*)string at:(CGPoint)p color:(Vector)color alpha:(float)alpha {
    const char* singleChars = [string cStringUsingEncoding:NSUTF8StringEncoding];
    CGPoint refPoint = p;
    for (int i = 0; i < [string length]; i++) {
        char object = singleChars[i];
        unsigned int key = object;
        //        printf("String: %c\nNumber: %i\n\n", object, key);
        float offset = WIDTH/3;
        if (key ==  32) { offset/=2; refPoint.x+=offset; continue; }
        
        GLshort vertices [] = {
            0, HEIGHT-HEIGHT/3,          //links unten
            WIDTH-WIDTH/3, HEIGHT-HEIGHT/3,      //rechts unten
            0, 0,               //links oben
            WIDTH-WIDTH/3, 0            //rechts oben
        };
        GLshort texCoords [] = {
            0, 1,   //links unten
            1, 1,   //rechts unten
            0, 0,   //links oben
            1, 0    //rechts oben
        };
        //glEnable(GL_TEXTURE_2D);
        NSNumber* num = [self.fontTextures objectForKey:[NSNumber numberWithUnsignedInt:key]];
        glBindTexture(GL_TEXTURE_2D, [num unsignedIntValue]);
        glVertexPointer(2, GL_SHORT, 0, vertices);
        glTexCoordPointer(2, GL_SHORT, 0, texCoords);
        
        glPushMatrix();
        
        glTranslatef(refPoint.x, refPoint.y, 0);
        glColor4f(color.x, color.y, color.z, alpha);
        glColorMask(true, true, true, true);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glColor4f(1, 1, 1, 1);
        glColorMask(true, true, true, true);
        
        glPopMatrix();
        
        //glDisable(GL_TEXTURE_2D);
        //        if (singleChars[i+1] == 105) { offset*=1.2; }
        //        if (singleChars[i+1] == 33)  { offset*=0.8; }
        //        if (key == 105 || key == 33 || key == 116 || key == 108) { offset/=2; }
        //        if (key == 114) { offset = offset-offset/3; }
        refPoint.x+=offset;  //Advance more to the left
    }
}

#pragma mark -
#pragma mark Helper
- (GLuint)createTextureFromSingleChar:(const char*)string {    
    CGSize size = CGSizeMake(RESX, RESY);     //Size of a single Character
    
    GLubyte *pixelData = (GLubyte *) calloc( size.width*size.height*4, sizeof(GLubyte) );
    CGColorSpaceRef rgbCS = CGColorSpaceCreateDeviceRGB();
    CGContextRef gc = CGBitmapContextCreate( pixelData, 
                                            size.width, size.height, 8, size.width*4,                                                       
                                            rgbCS, 
                                            kCGImageAlphaPremultipliedLast);        
    int fontsize = FONTSIZE; //Font-Groesse, kleiner als height
    CGContextSetRGBFillColor(gc, 1,1,1,1);
    CGContextSelectFont(gc, "Courier Bold", fontsize, kCGEncodingMacRoman);
    int ys = size.height-fontsize; //swapped y-axis
    CGContextShowTextAtPoint(gc, 0, ys, string, 1); //Render text auf gc  
    CGColorSpaceRelease(rgbCS);
    CGContextRelease(gc);
    
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.width, size.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelData);                     
    
    //Textur-bezogene States global aktivieren
//    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    //Cleanup
    free(pixelData);
    return textureID;
}

- (void)createStringArray {
    //Create all 95 usefull Ascii Chars
    for (unsigned int i = 33; i < 128; i++) {
        const char str = i;
        [self.fontTextures setObject:[NSNumber numberWithUnsignedInt:[self createTextureFromSingleChar:&str]]
                              forKey:[NSNumber numberWithUnsignedInt:i]];
    }
}




//- (GLuint) createTexture: (GLubyte*)pixelData {
//    GLuint textureID;
//    glGenTextures(1, &textureID);
//    glBindTexture(GL_TEXTURE_2D, textureID);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, WIDTH*CHARCOUNT, HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelData);                     
//    
//    //Textur-bezogene States global aktivieren
//    glEnable(GL_BLEND);
//    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
//    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//    return textureID;
//}
//
//- (GLubyte *) generatePixelDataFromFont: (NSString *) font {
//    NSString* string = @"";
//    unichar currentUnichar = 'a';
//    for (int i = 0; i < CHARCOUNT; i++) {
//        string = [string stringByAppendingString:[NSString stringWithFormat:@"%c", currentUnichar]];
//        currentUnichar++;
//    }
//    const char *cText = [string cStringUsingEncoding: NSASCIIStringEncoding];
//    GLubyte *pixelData = (GLubyte *) calloc( [self makePowerOfTwo:WIDTH*HEIGHT*4], sizeof(GLubyte) );
//    CGColorSpaceRef rgbCS = CGColorSpaceCreateDeviceRGB();
//    CGContextRef gc = CGBitmapContextCreate( pixelData, [self makePowerOfTwo:WIDTH], HEIGHT, 8, [self makePowerOfTwo:WIDTH*4], rgbCS, kCGImageAlphaPremultipliedLast);        
//    CGContextSetRGBFillColor(gc, 1.0,1.0,1.0,1); //Schriftfarbe
//    CGContextSelectFont(gc, [font cStringUsingEncoding:NSASCIIStringEncoding], FONTSIZE, kCGEncodingMacRoman);
//    int ys = HEIGHT-FONTSIZE; //swapped y-axis
//    CGContextShowTextAtPoint(gc, 0, ys/2, cText, strlen(cText)); //Render text auf gc   
//    CGColorSpaceRelease(rgbCS);
//    CGContextRelease(gc);    
//    return pixelData;
//}
//
//- (void)loadFont:(NSString*)font {
//    [fontTextures setObject:[[NSNumber alloc] initWithInt:[self createTexture:[self generatePixelDataFromFont:font]]] 
//                     forKey:font]; //NSNumber representing the textureID
//}
//
////helper
//- (float)makePowerOfTwo:(float)number {
//    float powerOf2 = 1;
//    while (powerOf2 < number) {
//        powerOf2*=2;
//    }
//    return powerOf2;
//}
//
//- (void)drawStringWithRect:(CGRect)rect andFont:(NSString*)font string:(NSString*)string {
//    float offset = 0.0;
//    GLshort vertices [] = {
//        0.0, HEIGHT,
//        WIDTH, HEIGHT,
//        0.0, 0.0,
//        WIDTH, 0.0,
//    };
//    glEnable(GL_TEXTURE_2D);
//    
//    for (int i = 0; i < [string length]; i++) {
//        GLfloat textureCoords[ ] = {                 
//            (1/CHARCOUNT)*([string characterAtIndex:i]-32)-(1/CHARCOUNT),  1.0, //links unten
//            (1/CHARCOUNT)*([string characterAtIndex:i]-32)              ,  1.0, //rechts unten
//            (1/CHARCOUNT)*([string characterAtIndex:i]-32)-(1/CHARCOUNT),  0.0, //links oben
//            (1/CHARCOUNT)*([string characterAtIndex:i]-32)              ,  0.0  //rechts oben                      
//        };
//        glColor4f(1, 1, 1, 1);
//        glBindTexture(GL_TEXTURE_2D, [[self.fontTextures objectForKey:font] intValue]);
//        glVertexPointer(2, GL_SHORT, 0, vertices);	
//        glTexCoordPointer(2, GL_FLOAT, 0, textureCoords);
//        
//        glPushMatrix();
//        glTranslatef(offset+rect.origin.x, rect.origin.y, 0.0);
//        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);         
//        glPopMatrix();
//        
//        offset+=WIDTH;
//    }
//    glDisable(GL_TEXTURE_2D);
//}

@end
