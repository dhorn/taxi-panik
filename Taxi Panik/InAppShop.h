//
//  InAppShop.h
//  Taxi Panik
//
//  Created by Dominik Horn on 13.03.13.
//
//

#import <StoreKit/StoreKit.h>
#import "MenuElement.h"
#import "InAppShopElement.h"

#define WHEELS_25 @"com.TaxiPanik.inApp.25Wheels"
#define WHEELS_100 @"com.TaxiPanik.inApp.100Wheels"
#define WHEELS_1000 @"com.TaxiPanik.inApp.1000Wheels"

@interface InAppShop : MenuElement <SKRequestDelegate, SKProductsRequestDelegate, InAppStoreElementDelegate> {
//    NSArray* _storeObjects;
    NSMutableArray* inAppStoreElements;
    float xScroll;
}

//@property (nonatomic, copy) NSArray* storeObjects;

@end
