//
//  Enemy.m
//  Taxi Panik
//
//  Created by Dominik Horn on 16.03.13.
//
//

#import "Enemy.h"
#import "Chunk.h"
#import "GameManager.h"
#import "Level.h"
#import "Math.h"

@implementation Enemy
@synthesize pathPoints;

- (id)init
{
    self = [super init];
    if (self) {
        self.pathPoints = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithTexture:(NSString *)texture rect:(CGRect)r speed:(Vector)s ai:(int)ai angle:(float)a {
    if (self = [super initWithTexture:texture rect:r speed:s ai:ai angle:a]) {
        self.pathPoints = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)findPathTo:(CGPoint)point {
    self.pathPoints = [[NSMutableArray alloc] initWithCapacity:20];
    
    NSMutableArray* chunks = [[GameManager getInstance].level chunks];
    
    Chunk* collisionChunk = nil;    /* Find the Chunk with wich we're colliding */
    for (Chunk* chunk in chunks) {
        if ([chunk isKindOfClass:[StreetChunk class]]) {
            if (aabbCol(self, chunk)) {
                collisionChunk = chunk;
                break;
            }
        }
    }
    
    /* Find the way */
    [self.pathPoints addObject:collisionChunk];
    while (!pointRectCol(((Chunk*)[self.pathPoints lastObject]).rect, point)) {   /* While we haven't found the way */
        Chunk* chunkToAdd = nil;
        int minDistance = 512*512;
        
        for (Chunk* chunk in ((Chunk*)[self.pathPoints lastObject]).neighboors) {
            if (![chunk isKindOfClass:[StreetChunk class]]) continue;
            /* Make sure we don't add pathpoints twice */
            BOOL skip = false;
            for (Chunk* chunkToCheck in self.pathPoints) {
                if (chunkToCheck == chunk) {
                    skip = true;
                }
            }
            if (skip) continue;
            
            /* Check if the current chunk is closer than the ones before */
            int distance = squareDistCCOfRects(chunk.rect, CGRectMake(point.x, point.y, 1, 1));
            if (distance < minDistance) {
                minDistance = distance;
                chunkToAdd = chunk;
            }
        }
        
        if (chunkToAdd) {
            [self.pathPoints addObject:chunkToAdd];
        } else {
            NSLog(@"Error, hit dead end");
        }
    }
}

- (void)update {
    NSArray* tmpPath = [NSArray arrayWithArray:self.pathPoints];
    Vector direction = makeVector(0, 0, 0);
    Vector playerChunk = makeVector(0, 0, 0);
    
    for (Chunk* chunk in tmpPath) {
        if (aabbCol(chunk, self)) {
            if (chunk != [tmpPath lastObject]) {
                Chunk* nextChunk = [tmpPath objectAtIndex:[tmpPath indexOfObject:chunk]+1];
                direction.x = nextChunk.rect.origin.x - chunk.rect.origin.x;
                direction.y = nextChunk.rect.origin.y - chunk.rect.origin.y;
                playerChunk = makeVector(nextChunk.rect.origin.x - rect.origin.x, nextChunk.rect.origin.y - rect.origin.y, 0);
            }
            break;
        } else {
            /* Remove unneeded chunks */
            [self.pathPoints removeObject:chunk];
        }
    }
    angle += angle2D(direction, speed) * (sqrtf(speed.x*speed.x + speed.y*speed.y) / sqrtf(playerChunk.x*playerChunk.x + playerChunk.y*playerChunk.y));
    
    [super update];
}

- (void)move {
    speed.x =  sin(getRad(self.angle));
    speed.y = -cos(getRad(self.angle));
    float gspeed = 0;
    
    switch (gameSpeed) {
        case SLOW:
            gspeed = SPEED_SLOW;
            break;
        case MEDIUM:
            gspeed = SPEED_MEDIUM;
            break;
        case FAST:
            gspeed = SPEED_FAST;
            break;
        case INSANE:
            gspeed = SPEED_INSANE;
            break;
        default:
            NSLog(@"Invalid Game Speed set!!!");
            break;
    }
    
    speed.x *= gspeed;
    speed.y *= gspeed;
    [super move];
}

@end
