//
//  GameObject.m
//  Taxi Panik
//
//  Created by Dominik Horn on 09.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameObject.h"
#import "Math.h"
#import "GameManager.h"

@implementation GameObject
@synthesize rect, angle, speed, colrect, textureName;

- (CollisionBitmap*)getCollisionBitmap {
    if (!colBitmap) {
        @try {
            CGImageRef imageRef = [[UIImage imageNamed:self.textureName] CGImage];
            NSUInteger width = CGImageGetWidth(imageRef);
            NSUInteger height = CGImageGetHeight(imageRef);
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            unsigned char *rawData = malloc(height * width * 4);
            NSUInteger bytesPerPixel = 4;
            NSUInteger bytesPerRow = bytesPerPixel * width;
            NSUInteger bitsPerComponent = 8;
            CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                         bitsPerComponent, bytesPerRow, colorSpace,
                                                         kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
            CGColorSpaceRelease(colorSpace);
            
            CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
            CGContextRelease(context);
            
            int colum, row; row = 0; colum = 0;
            for (int count = 0 ; count < width*height*4 ; count+=4)
            {
                colum++;
                if (colum == width-1) {
                    colum = 0;
                    row++;
                }
                CGFloat red   = (rawData[count]     * 1.0) / 255.0;
                CGFloat green = (rawData[count + 1] * 1.0) / 255.0;
                CGFloat blue  = (rawData[count + 2] * 1.0) / 255.0;
                CGFloat alpha = (rawData[count + 3] * 1.0) / 255.0;
                Bit* bit = [[Bit alloc] init]; bit.color = [UIColor colorWithRed:red
                                                                           green:green
                                                                            blue:blue
                                                                           alpha:alpha];
                bit.solid = alpha ? true : false;
                bit.position = CGPointMake(colum, row);
                [colBitmap addBit:bit];
//                NSLog(@"Position: %f %f", bit.position.x, bit.position.y);
            }
            
            free(rawData);
        }
        @catch (NSException *exception) {
            NSLog(@"ERROR: PICTURE NOT FOUND");
        }
    }
    return colBitmap;
}

- (void)move {
    rect.origin.x+=self.speed.x*deltatime;
    rect.origin.y+=self.speed.y*deltatime;
}

- (void)update {
    //To be overriden
 }

- (float)getRadius {
    if (radius == 0.0) {
        CGPoint center = CGPointMake(rect.size.width/2+rect.origin.x, rect.size.height/2+rect.origin.y);
        radius = (center.x-rect.origin.x)*(center.x-rect.origin.x)+(center.y-rect.origin.y)*(center.y-rect.origin.y);
    }
    return radius;
}

- (void)setRadius:(float)r {
    radius = r;
}

@end
