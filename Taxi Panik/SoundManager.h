//
//  SoundManager.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "CustomAVAudioPlayer.h"

@interface SoundManager : NSObject <AVAudioPlayerDelegate> {
    BOOL isMultithreaded;
    NSThread* soundThread;
    
    NSMutableDictionary* sounds;
    
    BOOL isOnHold;
}

//class methods
+ (SoundManager*)getInstance;

- (void)shutDownAudioSystem;

//Methodes
- (BOOL)isPlaingsoundOnSecondThread;
- (void)playSoundOnSecondThread:(BOOL)v;

- (void)loopSound:(NSString*)name;
- (void)playSound:(NSString*)name;
- (void)playMusic:(NSString*)name;
- (void)stopSound:(NSString*)name;
- (void)play:(CustomAVAudioPlayer*)s;
- (void)generateRandomThemeSongName:(NSString**)name;


- (void)pauseMusic;
- (void)unPauseMusic;
- (void)mute;
- (void)unMute;

//Second thread
- (void)blockUntilNotified;

//intern
- (CustomAVAudioPlayer*)getSound:(NSString*)name;

@property (nonatomic, retain) NSMutableDictionary* sounds;
@property BOOL isMultithreaded;

@end
