//
//  GameManager.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <OpenGLES/ES1/gl.h>
#import <GameKit/GameKit.h>

#import "LevelLoader.h"
#import "SoundManager.h"
#import "MenuManager.h"
#import "Font.h"
#import "Math.h"
#import "OPoint.h"

#import "AI.h"
#import "Sprite.h"
#import "Car.h"
#import "Player.h"
#import "Customer.h"
#import "Particle.h"
#import "Animation.h"
#import "Enemy.h"


extern int w;
extern int h;
extern int gameSpeed;
extern float fps;
extern NSTimeInterval deltatime;

#define SOUND_THEME_1       @"Taxi_Panik Theme_1.mp3"
#define SOUND_THEME_2       @"Taxi_Panik Theme_2.mp3"
#define SOUND_THEME_3       @"Taxi Panik Theme 3.mp3"
#define SOUND_EXPLOSION     @"Explosion.wav"
#define SOUND_BUTTON_PRESS  @"Button_Press.wav"
#define SOUND_SQUEAL        @"squeal.mp3"

#define BUTTON_START NSLocalizedString(@"START", nil)
#define BUTTON_OPTIONS NSLocalizedString(@"OPTIONS", nil)
#define BUTTON_CREDITS NSLocalizedString(@"CREDITS", nil)
#define BUTTON_MUSIC_ON NSLocalizedString(@"MUSIC_ON", nil)
#define BUTTON_MUSIC_OFF NSLocalizedString(@"MUSIC_OFF", nil)
#define BUTTON_SFX_ON NSLocalizedString(@"SFX_ON", nil)
#define BUTTON_SFX_OFF NSLocalizedString(@"SFX_OFF", nil)
#define BUTTON_BACK NSLocalizedString(@"BACK", nil)
#define BUTTON_MENU NSLocalizedString(@"MENU", nil)
#define BUTTON_SCORE NSLocalizedString(@"SCORE", nil)
#define BUTTON_INAPP NSLocalizedString(@"INAPP", nil)
#define BUTTON_PAUSE @"||"
#define BUTTON_STORE NSLocalizedString(@"STORE", nil)

#define KEY_SCORES @"SCORES"
#define KEY_MUSIC @"MUSIC_ENABLED"
#define KEY_SOUND @"SOUND_ENABLED"
#define KEY_CURRENCY @"MONEY"

#define SCORE_LIMIT 13
#define AUTO_SAVE_TIME 30   //Save every 5 seconds

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IPHONE5_SCALE 1.1833
#define IPHONE5_TRANS 44

enum speed {
    SLOW,
    MEDIUM,
    FAST,
    INSANE
};

enum states {
    LOAD_GAME,
    START_GAME,
    GAME_LOAD_CHOOSE_LEVEL,
    GAME_CHOOSE_LEVEL,
    PLAY_GAME,
    LOAD_WON_GAME,
    WON_GAME,
    LOAD_CREDITS,
    GAME_CREDITS,
    LOAD_OPTIONS,
    GAME_OPTIONS,
    LOAD_SCORE,
    GAME_SCORE,
    LOAD_INAPP,
    GAME_INAPP,
    LOAD_STORE,
    GAME_STORE,
};

enum vbos {
    CHUNK_VBO,
};

#define PLAYERDEAD -500
#define SPEEDUP 0
#define CUSTOMER_SERVED 50
#define CUSTOMER_ANGRY -15

#define SPAMWITHDACUSTOMA false

#define CanControllCarWhenRotating false

#define EASTEREGGCOMBILOCKCOUNT 4

@interface GameManager : NSObject <GKGameCenterControllerDelegate, UIAlertViewDelegate> {
    int state, timer;
    NSDate* previous;
    float time;
    int fpscounter, fps;
    
    Level* currentLevel;
    
    NSMutableArray* destroyable;
    NSMutableArray* objectsToAdd;
    NSMutableArray* gameObjects;
    
    NSTimer* zoom;
    float ztimetracker;
    float currentzoom;
    
    //tmp
    bool toggle, hasCar;
    //    BOOL zoomed;
    
    int score;
    
    float customerSpawnTimer;
    float increaseDifficultyTimer;
    
    float creditsFade;
    float creditsTimer;
    
    //Menu
    BOOL musicOn;
    BOOL sfxOn;
    
    BOOL developer;
    BOOL easterEgg;
    int currentIndex;
    CGRect easterEggArray[EASTEREGGCOMBILOCKCOUNT];   //4 "Digit" Combination Lock ;)
    
    BOOL running;
    
    NSArray* levelArray;
    
    int timeinterval;
    
    float accelX, accelY;
    
    float playGameCountdown;
    BOOL startGame;
    
    //    Sprite* background;
    
    NSTimer* accelTimeOut;
    
    NSMutableArray* scores;
    
    BOOL fadeIn, fadeOut;
    float fadeAlpha;
    
    BOOL paused;
    
    float autoSaveTimer;

    int chunkTypes[16]; // This array is used for the iPhone 5 screen decoration
    int carsInGame;
    int carsThatCount;
    
    int steeringWheels; /* Ingame Currency */
    int costToContinue;
    
    int spawnWheelTimer;    /* Count down which is randomly set, so that you can gain 1 Wheel about every 25s */
}

//class methods
+ (GameManager*)getInstance;
- (void)preloader;

//game methods
- (void)touchesBegan:(NSSet*)touches inView:(UIView*)view;
- (void)touchesMoved:(NSSet*)touches inView:(UIView*)view;
- (void)touchesEnded:(NSSet*)touches inView:(UIView*)view;
- (void)touchesCancelled:(NSSet*)touches inView:(UIView*)view;
- (void)acceleration:(UIAcceleration*)accel;
- (void)playGame;
- (void)spawnEntityOnRandomTick;
- (void)buttonEvent:(NSString*)bt;
- (void)toggleStates;

//drawing methods
- (void)drawStatesWithFrame:(CGRect)frame;
- (void)drawOGLLinefrom:(CGPoint)p1 to:(CGPoint)p2 color:(Vector)vec alpha:(float)alpha;
- (void)drawOGLRect:(CGRect)rect color:(Vector)v alpha:(float)a;

//helper methods
- (void)manageSprites;
- (void)renderSprites;
- (void)manageCollision:(GameObject*)object;
- (void)setOGLProjection;
- (void)loadLevel:(NSString*)name;
- (void)fps;
- (void)animatedZoom:(float)ztv;
- (void)zoom:(float)factor;

//- (void)setupMenu;
- (BOOL)isMusicOn;
- (BOOL)isSfxOn;

- (void)start;
- (void)stop;
- (BOOL)running;

- (void)scrollViewClick:(int)index;
- (int)levelCount;
- (NSString*)nameForIndex:(int)index;

- (void)timeOutAccel;

- (void) saveObject: (id) object key: (NSString *) key;
- (id) readObjectforKey: (NSString *) key;

- (void)addToScore:(int)amount;

- (void)saveData;
- (void)loadData;
- (void)pause;

@property (nonatomic, retain) NSMutableArray* objectsToAdd;
@property int speed;
@property int state;
@property int countDownNumber;
@property (readonly) Level* level;

@end
