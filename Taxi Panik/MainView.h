//
//  MainView.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <QuartzCore/QuartzCore.h>
#import "GameManager.h"

@interface MainView : UIView <UIAccelerometerDelegate> {
    EAGLContext *eaglContext; 
    GLuint renderbuffer;
    GLuint framebuffer;
    GLuint depthbuffer; 
    GLint viewportWidth;
    GLint viewportHeight;

    GameManager* gameManager;
}

- (void) setupOGL;

@end
