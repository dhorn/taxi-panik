//
//  MainView.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainView.h"

@implementation MainView
+ (Class)layerClass {
    return [CAEAGLLayer class];
}

- (void)displayLayer:(CALayer *)layer {
    //Doesn't matter, won't do anything anyway, only spamming log
}

- (void) setupOGL {
    CAEAGLLayer *eaglLayer = (CAEAGLLayer *) self.layer;
    eaglLayer.opaque = YES;
    
    eaglContext = [[EAGLContext alloc] initWithAPI: kEAGLRenderingAPIOpenGLES1];    
    if (!eaglContext || ![EAGLContext setCurrentContext: eaglContext]) {
        NSLog(@"ERROR, COULDN'T GENERATE VIEW");
        return;
    } else {    
        //Renderbuffer 
        glGenRenderbuffersOES(1, &renderbuffer);
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, renderbuffer);
        
        //Framebuffer 
        glGenFramebuffersOES(1, &framebuffer);
        glBindFramebufferOES(GL_FRAMEBUFFER_OES, framebuffer);        
        glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, renderbuffer);
        
        //Graphic context  
        [eaglContext renderbufferStorage: GL_RENDERBUFFER_OES fromDrawable: eaglLayer];
        glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &viewportWidth);
        glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &viewportHeight);
        
        //Depthbuffer (3D only)
        glGenRenderbuffersOES(1, &depthbuffer);
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, depthbuffer);       
        glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, viewportWidth, viewportHeight);                
        glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthbuffer);        
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, renderbuffer); //rebind
    }
    
    if (!gameManager) {
        gameManager = [GameManager getInstance];
    }
    glDisable(GL_LIGHTING);
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [UIAccelerometer sharedAccelerometer].delegate = self;
        [UIAccelerometer sharedAccelerometer].updateInterval = 1.0 / 30.0;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    if (![gameManager running]) return;
    glViewport(0, 0, viewportWidth, viewportHeight);    
    //fuer 2D reicht glClear(GL_COLOR_BUFFER_BIT); da GL_DEPTH_TEST disabled 
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    
//    glDisable(GL_TEXTURE_2D);
//    GLshort vertices[] = {
//        0.0, 0.0,
//        HEIGHT, 0.0,
//        WIDTH, HEIGHT,
//    };
//    
////    glColor4f(1.0, 1.0, 1.0, 1.0);
//    glVertexPointer(2, GL_SHORT, 0, vertices);
//    glDrawArrays(GL_TRIANGLES, 0, 3);
    
    [gameManager drawStatesWithFrame: rect];
    [eaglContext presentRenderbuffer: GL_RENDERBUFFER_OES];      
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // => Not Implemented/perhaps not needed 
    //[self setMultipleTouchEnabled:YES]; 
    [gameManager touchesBegan:touches inView:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [gameManager touchesMoved:touches inView:self];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [gameManager touchesEnded:touches inView:self];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [gameManager touchesCancelled:touches inView:self];
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
    [gameManager acceleration:acceleration];
}

@end
