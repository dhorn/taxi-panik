//
//  GameObject.h
//  Taxi Panik
//
//  Created by Dominik Horn on 09.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/gl.h>
#import "TextureManager.h"
#import "Math.h"
#import "CollisionBitmap.h"
#import "Bit.h"

//Standard gameObject Protocol, every GameObject must have these properties
@interface GameObject : NSObject {
    CGRect colrect;
    CGRect rect;
    Vector speed;
    float angle;
    float radius; //has to be squared to work
    CGRect texCoords;

    NSString* textureName;
    CollisionBitmap* colBitmap;
    float texCounter;
}

@property CGRect colrect;
@property CGRect rect;
@property Vector speed;
@property float angle;
@property (nonatomic, retain) NSString* textureName;

- (CollisionBitmap*)getCollisionBitmap;
- (void)update;
- (void)move;

- (float)getRadius;
- (void)setRadius:(float)r;

//Math 

@end
