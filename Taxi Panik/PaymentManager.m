//
//  PaymentManager.m
//  Taxi Panik
//
//  Created by Dominik Horn on 13.03.13.
//
//

#import "PaymentManager.h"
#import "InAppShop.h"
#import "GameManager.h"

@implementation PaymentManager

- (void)paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray *)downloads {
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction* trans in transactions) {
        if (trans.transactionState == SKPaymentTransactionStatePurchased) {
            [self delieverContent:trans.payment.productIdentifier];
            [[SKPaymentQueue defaultQueue] finishTransaction: trans];
        }
        if (trans.transactionState == SKPaymentTransactionStateFailed) {
            if (trans.error.code != SKErrorPaymentCancelled) {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"COULDNTPURCHASE", nil)
                                            message:[NSString stringWithFormat:NSLocalizedString(@"COULDNTPURCHASEERROR", nil),
                                                     trans.transactionIdentifier,
                                                     trans.error]
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedString(@"DISMISS", nil)
                                  otherButtonTitles:nil] show];
            }
            [[SKPaymentQueue defaultQueue] finishTransaction: trans];
        }
    }
}

- (void)delieverContent:(NSString*)identifier {
    if ([identifier isEqualToString:WHEELS_25]) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SUCCESS", nil)
                                    message:NSLocalizedString(@"250WHEELSADDED", nil)
                                  delegate:nil
                         cancelButtonTitle:@"Cool"
                         otherButtonTitles:nil] show];
        [[GameManager getInstance] addToScore:250];
    }
    else if ([identifier isEqualToString:WHEELS_100]) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SUCCESS", nil)
                                    message:NSLocalizedString(@"1000WHEELSADDED", nil)
                                   delegate:nil
                          cancelButtonTitle:@"Cool"
                          otherButtonTitles:nil] show];
        [[GameManager getInstance] addToScore:1000];
    }
    else if ([identifier isEqualToString:WHEELS_1000]) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SUCCESS", nil)
                                    message:NSLocalizedString(@"10000WHEELSADDED", nil)
                                   delegate:nil
                          cancelButtonTitle:@"Cool"
                          otherButtonTitles:nil] show];
        [[GameManager getInstance] addToScore:10000];
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", nil)
                                    message:[NSString stringWithFormat:NSLocalizedString(@"PLEASECONTACTSUPPORT", nil), identifier]
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(@"DISMISS", nil)
                          otherButtonTitles:nil] show];
    }
}

@end
