//
//  Player.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"

@class Chunk;

#define SPEED_SLOW 25.0
#define SPEED_MEDIUM 35.0
#define SPEED_FAST 40.0
#define SPEED_INSANE 45.0

@interface Player : Car {
    BOOL displayOldPathPoints;
    BOOL rotate;
    float rotateTo, deltaAngle;
    int tickcount;
    int texTick;
    
    float blinkCounter;
    
    
    NSMutableArray* _oldpathPoints;
    NSMutableArray* _pathPoints;
    
    Chunk* currentChunkToRotateOn;
    Chunk* nextChunk;

    float initAppearTimer;
}

- (void)toggleDisplayOldPathPoints;
- (BOOL)rotate;

@property (readonly) BOOL displayOldPathPoints;
@property float initAppearTimer;
@property (nonatomic, retain) NSMutableArray* oldPathPoints;
@property (nonatomic, retain) NSMutableArray* pathPoints;

@end
