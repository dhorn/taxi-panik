//
//  OPoint.m
//  Taxi Panik
//
//  Created by Dominik Horn on 15.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OPoint.h"

@implementation OPoint
@synthesize pos;

+ (OPoint*)pwp:(CGPoint)p {
    OPoint* po = [[OPoint alloc] init];
    po.pos = p;
    return po;
}

@end
