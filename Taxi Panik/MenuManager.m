//
//  MenuManager.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuManager.h"
#import "GameManager.h"

@implementation MenuManager

#pragma mark -
#pragma mark class Methods
+ (MenuManager*)getInstance {
    static MenuManager* menuManager;
    if (!menuManager) {
        menuManager = [[MenuManager alloc] init];
        [menuManager preloader];
    }
    return menuManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        guiElements = [[NSMutableArray alloc] initWithCapacity:10]; //10 Gui Elements is more than enough
    }
    return self;
}

- (void)preloader {
    //Texture pack stuff?
}

- (void)addElement:(int)element rect:(CGRect)rect angle:(float)angle title:(NSString *)title {
    if (element == BUTTON) {
        Button* b = [[Button alloc] init]; b.rect = rect; b.angle = angle; b.textureName = @"Button.png"; b.title = title; 
        [guiElements addObject:b];
    } else if (element == SCROLLVIEW) {
        ScrollView* s = [[ScrollView alloc] initWithRect:rect]; s.angle = angle; s.title = title;
        [guiElements addObject:s];
    } else if (element == INAPP) {
        InAppShop* shop = [[InAppShop alloc] init];
        [guiElements addObject:shop];
    } else {
        NSLog(@"ERROR: UNKNOWN MENUELEMENT TYPE!");
    }
}

- (void)touch:(CGPoint)t point2:(CGPoint)p2 event:(int)ev {
    for (MenuElement* element in guiElements) {
        if ([element receiveEvent:ev point:t point2:p2])
            break;
    }
}

- (void)render {
    for (MenuElement* element in guiElements) {
        [element render];
    }
//    [[GameManager getInstance] drawStrings];
}

- (void)destroy {
    [guiElements removeAllObjects];
}

@end
 