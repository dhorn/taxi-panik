//
//  Font.h
//  Taxi Panik
//
//  Created by Dominik Horn on 28.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/gl.h>
#import "Math.h"

#define WIDTH 32        //This is NOT the resolution
#define HEIGHT 32

#define RESX 32
#define RESY 32
#define FONTSIZE 26

@interface Font : NSObject {
    NSMutableDictionary* fontTextures;
}

+ (Font*)getInstance;

- (void)drawString:(NSString*)string at:(CGPoint)p;
- (void)drawColoredString:(NSString*)string at:(CGPoint)p color:(Vector)color;
- (void)drawColoredString:(NSString*)string at:(CGPoint)p color:(Vector)color alpha:(float)alpha;

//Helper
- (GLuint)createTextureFromSingleChar:(const char*)string;
- (void)createStringArray;

@property (nonatomic, retain) NSMutableDictionary* fontTextures;

@end
