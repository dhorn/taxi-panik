//
//  Sprite.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameObject.h"
#import "AI.h"

enum SpriteTypes {
    CAR,
    PLAYER,
    CUSTOMER
};

@interface Sprite : GameObject {
    int type;
    int aiType;
    
    BOOL isActive;
}

- (id)initWithTexture:(NSString*)texture
                 rect:(CGRect)r
                speed:(Vector)s
                   ai:(int)ai
                angle:(float)a;

- (void)render;
- (void)setType:(int)t;
- (int)getType;

@property int aiType;
@property BOOL isActive;

@end
