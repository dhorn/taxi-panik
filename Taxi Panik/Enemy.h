//
//  Enemy.h
//  Taxi Panik
//
//  Created by Dominik Horn on 16.03.13.
//
//

#import "Car.h"
#import "Player.h"
@class Chunk;

@interface Enemy : Car {
    
}

- (void)findPathTo:(CGPoint)point;

@property (nonatomic, retain) NSMutableArray* pathPoints;

@end
