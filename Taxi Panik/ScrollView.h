//
//  ScrollView.h
//  Taxi Panik
//
//  Created by Dominik Horn on 13.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuElement.h"
@class ScrollViewElement;

#define ELEMENTHEIGHT 75
#define SCROLLVIEWSIZE ELEMENTHEIGHT
#define BUTTONOFFSET 20

@interface ScrollView : MenuElement {
    NSMutableArray* _elements;
    ScrollViewElement* _selectedElement;
    
    float refPointY;    //Our scroll View only supports Y Scrolling
}

- (id)initWithRect:(CGRect)rect;

@property (nonatomic, retain) ScrollViewElement* selectedElement;
@property (nonatomic, retain) NSMutableArray* elements;

@end
