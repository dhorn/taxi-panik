//
//  Animation.m
//  Taxi Panik
//
//  Created by Dominik Horn on 08.12.12.
//
//

#import "Animation.h"
#import "GameManager.h"

@implementation Animation

- (id)init
{
    self = [super init];
    if (self) {
        animationTimer = 0;
        timeCounter = 0;
        texCoords = CGRectMake(0.0, 0.0, 1.0/8.0, 1.0);
    }
    return self;
}

- (void)update {
    [super update];
    timeCounter += deltatime;
    if (timeCounter > 0.1) {
        timeCounter -= 0.1;
        animationTimer++;
        if (animationTimer >= 8) isActive = false;
    }
    texCoords = CGRectMake((1.0/8.0)*animationTimer, 0.0, 1.0/8.0+(1.0/8.0)*animationTimer, 1.0);
}

@end
