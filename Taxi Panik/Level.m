//
//  Level.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Level.h"
#import "StreetChunk.h"
#import "GameManager.h"

@implementation Level
@synthesize chunks, chunkCounts, numPlayers;

- (id)init
{
    self = [super init];
    if (self) {
        self.chunks = [NSMutableArray arrayWithCapacity:100];
        self.chunkCounts = [NSMutableDictionary dictionaryWithCapacity:7];
    }
    return self;
}

- (void)render {
    for (Chunk* chunk in self.chunks) {
        if ([GameManager getInstance].state == PLAY_GAME && [GameManager getInstance].countDownNumber <= 0 && [chunk isKindOfClass:[StreetChunk class]]) {
            if (((StreetChunk*)chunk).isFork) {
                [chunk update];
            }
        }
        [chunk render];
    }
}

@end
