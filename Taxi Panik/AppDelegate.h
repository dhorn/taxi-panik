//
//  AppDelegate.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>

#import "ViewController.h"
#import "PaymentManager.h"

#define FRAMERATE 2   //30 FPS => CADisplayLink

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    NSTimer* timer;
    CADisplayLink* gameTimer;
    ViewController* mainViewController;
    bool loop;
    
    GKLocalPlayer* _localPlayer;
}

- (void)loop;
- (void)startLoop;
- (void)stopLoop;

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) PaymentManager* payManager;   /* reach out to the PaymentManager through the App Delegate if really needed */
@property (nonatomic, retain) GKLocalPlayer* localPlayer;
@property BOOL gameCenterEnabled;

@end
