//
//  Level.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Chunk.h"

#define NUMSTREETCHUNKS @"0"
#define NUMGRASSCHUNKS @"1"
#define NUMHOUSECHUNKS @"2"

@interface Level : NSObject {
    NSMutableArray* chunks;
    NSMutableDictionary* chunkCounts;
    
    int numPlayers;
}

- (void)render;

@property (nonatomic, retain) NSMutableArray* chunks;
@property (nonatomic, retain) NSMutableDictionary* chunkCounts;
@property int numPlayers;

@end
