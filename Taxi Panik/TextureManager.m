//
//  TextureManager.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TextureManager.h"

//NSString* chars = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; //All letters to be used in the game

@implementation TextureManager

#pragma mark -
#pragma mark class Methods
+ (TextureManager*)getInstance {
    static TextureManager* textureManager;
    if (!textureManager) {
        textureManager = [[TextureManager alloc] init];
        [textureManager preloader];
    }
    return textureManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)preloader {
    //Generate String array
//    characters = [[NSMutableDictionary alloc] initWithCapacity:65];
//    for (int i = 0; i < [chars length]; i++) {
//        Texture* tex = [[Texture alloc] init];
//        tex.width = LETTERWIDTH;
//        tex.height = LETTERHEIGHT;
//        [self generateTexture:[self generatePixelDataFromString:[NSString stringWithFormat:@"%c", [chars characterAtIndex:i]]]
//                      texture:tex];
//        [characters setObject:tex forKey:[NSString stringWithFormat:@"%c", [chars characterAtIndex:i]]];
//    }
}

//Preparation for Texture Packs
- (void)loadDependencies:(NSString*)texPack {

    return;
    
    NSError* error;
    textureDependencies = [[NSMutableDictionary alloc] initWithCapacity:10];
    NSArray* textureLines = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:texPack ofType:@"txt"]
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error] componentsSeparatedByString:@"\n"];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    for (NSString* string in textureLines) {
        Texture* tex = [[TextureManager getInstance] getTextureObject:[[string componentsSeparatedByString:@": "] objectAtIndex:1]];
        [textureDependencies setObject:tex
                     forKey:[[string componentsSeparatedByString:@": "] objectAtIndex:0]];
    }
}

- (GLuint)getTextureID:(NSString*)name {
    Texture *tex = [[self getDictionary] objectForKey: name];
	if (!tex) {
		tex = [self getTextureObject:name];
        [[self getDictionary] setObject: tex forKey: name];
    }          
	return tex.textureID;
}

- (NSMutableDictionary*)getDictionary {
    if (!dictionary) { //Hashtable
		dictionary = [[NSMutableDictionary alloc] init]; 
	}
    return dictionary;    
}

- (Texture*)getTextureObject:(NSString*)name {
    UIImage *pic = [UIImage imageNamed:name];
    Texture* tex = [[Texture alloc] init];
    if (pic) {
        //Texturabmessungen festlegen
        tex.width = pic.size.width;
        tex.height = pic.size.height;
        if ( (tex.width & (tex.width-1)) != 0 || (tex.height & (tex.height-1)) != 0 
            || tex.width > 2048 || tex.height > 2048) {
            NSLog(@"ERROR: %@ width und/oder height ist keine 2er Potenz oder > 2048!", name); 
        }  
        
        //Pixeldaten erzeugen
        GLubyte *pixelData = [self generatePixelDataFromImage:pic texture:tex];    
        
        //Aus den Pixeldaten die Textur erzeugen und als ID speichern
        [self generateTexture:pixelData texture:tex]; 
        
        //Cleanup
        free(pixelData);
        //[pic release]; 
    } else {
        NSLog(@"ERROR: %@ nicht gefunden, Textur nicht erzeugt.", name);
    }
    return tex;
}

- (void) generateTexture:(GLubyte*)pixelData texture:(Texture*)tex {
    GLuint textureID = tex.textureID;
    glGenTextures(1, &textureID);
    tex.textureID = textureID;
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex.width, tex.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelData);                     
    
    //Textur-bezogene States global aktivieren
//    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}

- (GLubyte*)generatePixelDataFromImage:(UIImage*)pic texture:(Texture*)tex {     
    GLubyte *pixelData = (GLubyte *) calloc( tex.width*tex.height*4, sizeof(GLubyte) );
    CGColorSpaceRef imageCS = CGImageGetColorSpace(pic.CGImage);
    CGContextRef gc = CGBitmapContextCreate( pixelData, 
                                            tex.width, tex.height, 8, tex.width*4,                                                       
                                            imageCS, 
                                            kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(gc, CGRectMake(0, 0, tex.width, tex.height), pic.CGImage); //Render pic auf gc  
    CGColorSpaceRelease(imageCS);
    CGContextRelease(gc);
    return pixelData;    
}


//- (GLubyte*)generatePixelDataFromString:(NSString*)string {
//    //Texturabmessungen festlegen
//    int width;
//    int height;
//    int len = [string length]*LETTERWIDTH;
//    if (len < 64) width = 64;
//    else if (len < 128) width = 128;
//    else if (len < 256) width = 256;
//    else width = 512; //max width text    
//    height = LETTERHEIGHT;    
//    
//    //Pixeldaten erzeugen
//    const char *cText = [string cStringUsingEncoding: NSASCIIStringEncoding];
//    GLubyte *pixelData = (GLubyte *) calloc( width*height*4, sizeof(GLubyte) );
//    CGColorSpaceRef rgbCS = CGColorSpaceCreateDeviceRGB();
//    CGContextRef gc = CGBitmapContextCreate( pixelData,
//                                            width, height, 8, width*4,                                                       
//                                            rgbCS, 
//                                            kCGImageAlphaPremultipliedLast);        
//    int size = LETTERHEIGHT/2+2; //Font-Groesse, kleiner als height
//    CGContextSetRGBFillColor(gc, 0,0,0,1);
//    CGContextSelectFont(gc, FONT, size, kCGEncodingMacRoman);
//    int ys = height-size; //swapped y-axis
//    CGContextShowTextAtPoint(gc, 0, ys, cText, strlen(cText)); //Render text auf gc   
//    CGColorSpaceRelease(rgbCS);
//    CGContextRelease(gc);    
//    if (pixelData)
//        return pixelData;
//    else {
//        NSLog(@"ERROR: COULDN'T GENERATE PIXELDATA");
//    }
//    pixelData = NULL;
//    return pixelData;
//}

//- (void)drawString:(NSString*)string withRect:(CGRect)rect {
//    //Some kind of optimized, can still be done more efficient (Memory, functionOverhead etc)
//    glEnable(GL_TEXTURE_2D);
//    float offset = 0;
//    GLshort vertices [] = {
//        5.0, rect.size.height/string.length,
//        (rect.size.width-10)/string.length, rect.size.height/string.length,
//        5.0, 0.0,
//        (rect.size.width-10)/string.length, 0.0
//    };
//    
//    GLfloat texCoords [] = {
//        0.0, 1.0,
//        1.0, 1.0,
//        0.0, 0.0,
//        1.0, 0.0,
//    };
//    glColor4f(1, 1, 1, 1);
//    glVertexPointer(2, GL_SHORT, 0, vertices);	
//    glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
//    for (int i = 0; i < [string length]; i++) {
//        Texture* tex = [characters objectForKey:[NSString stringWithFormat:@"%c", [string characterAtIndex:i]]];
//        glBindTexture(GL_TEXTURE_2D, tex.textureID); 
//        glPushMatrix();
//        glTranslatef(rect.origin.x+offset, rect.origin.y, 0.0);
//        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
//        glPopMatrix();
//        offset+=rect.size.width/string.length;
//    }
//    Texture* tex = [characters objectForKey:@"a"];
//    if (!tex || ![tex isKindOfClass:[Texture class]])
//        NSLog(@"ERROR: COULDN'T GET TEXTURE OBJECT");
////    glBindTexture(GL_TEXTURE_2D, tex.textureID);
////    glPushMatrix();
////    glTranslatef(20.0, 20.0, 0.0);
////    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
////    glPopMatrix();
//    glDisable(GL_TEXTURE_2D);
//}

@end
