//
//  ScrollView.m
//  Taxi Panik
//
//  Created by Dominik Horn on 13.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ScrollView.h"
#import "Font.h"
#import "ScrollViewElement.h"
#import "GameManager.h"

@implementation ScrollView
@synthesize elements = _elements;
@synthesize selectedElement = _selectedElement;

- (id)init
{
    self = [super init];
    if (self) {
        self.elements = [[NSMutableArray alloc] init];
        self.selectedElement = nil;
        self.rect = CGRectMake(0, 0, 100, 100);
        
        [self loadLevels];
    }
    return self;
}

- (id)initWithRect:(CGRect)r {
    self = [super init];
    if (self) {
        self.elements = [[NSMutableArray alloc] init];
        self.selectedElement = nil;
        self.rect = r;
        
        [self loadLevels];
    }
    return self;
}

- (void)loadLevels {
    for (int i = 0; i < [[GameManager getInstance] levelCount]; i++) {
        [self.elements addObject:[ScrollViewElement scrollViewElementWithRect:CGRectMake(self.rect.origin.x,
                                                                                         self.rect.origin.y+i*(ELEMENTHEIGHT+BUTTONOFFSET),
                                                                                         self.rect.size.width,
                                                                                         ELEMENTHEIGHT)
                                                                         name:[NSString stringWithFormat:[[GameManager getInstance] nameForIndex:i], i]]];
    }

    
//    for (int i = 0; i < 5; i++) {
//        [self.elements addObject:[ScrollViewElement scrollViewElementWithRect:CGRectMake(self.rect.origin.x,
//                                                                                         self.rect.origin.y+i*(ELEMENTHEIGHT+BUTTONOFFSET),
//                                                                                         self.rect.size.width,
//                                                                                         ELEMENTHEIGHT)
//                                                                         name:[NSString stringWithFormat:@"%i", i]]];
//    }
}

- (BOOL)receiveEvent:(int)event point:(CGPoint)p point2:(CGPoint)p2 {
    switch (event) {
        case EVENTTOUCHDOWN:
        {
            ScrollViewElement* element = [self getCellAtPos:p];
            if (!element) return false; //Couldn't receive Element
            self.selectedElement = element;
            element.selected = true;
        }
            return true;
        case EVENTTOUCHMOVE:
            self.selectedElement.selected = false;
            self.selectedElement = nil;
            refPointY += (p.y - p2.y)/1.1;
            [self correctBounds];
            return true;
        case EVENTTOUCHUP:
            if (self.selectedElement) {
                [[GameManager getInstance] scrollViewClick:[self.elements indexOfObject:self.selectedElement]];
                [[SoundManager getInstance] playSound:SOUND_BUTTON_PRESS];
                self.selectedElement.selected = false;
                self.selectedElement = nil;
            }
            return true;
        case EVENTTOUCHCANCELED:
            if (self.selectedElement) {
                self.selectedElement.selected = false;
                self.selectedElement = nil;
            }
            return true;
        default:
            break;
    }
    return false;
}

- (void)correctBounds {
    if (refPointY > 0 || (self.elements.count*ELEMENTHEIGHT + (self.elements.count-1)*BUTTONOFFSET)-320.0 < 1)
        refPointY = 0;
    if ((self.elements.count*ELEMENTHEIGHT + (self.elements.count-1)*BUTTONOFFSET)-320.0 > 1
        && -refPointY > (self.elements.count*ELEMENTHEIGHT + (self.elements.count-1)*BUTTONOFFSET)-320.0)
        refPointY = -1.0*((float)(self.elements.count*ELEMENTHEIGHT + (self.elements.count-1)*BUTTONOFFSET)-320.0);
    
}

- (void)render {
    
    for (ScrollViewElement* element in self.elements) {
        if (![element isKindOfClass:[ScrollViewElement class]]) continue;
        
        if (rectRectCol(CGRectMake(element.rect.origin.x,
                                   element.rect.origin.y + refPointY,
                                   element.rect.size.width,
                                   element.rect.size.height), CGRectMake(0, 0, w, h))) {
            [element renderWithYOffset:refPointY];
        }
    }
    
    [self renderName];
}


- (void)renderName {
    glPushMatrix();
    [[Font getInstance] drawString:self.title at:CGPointMake(self.rect.origin.x, self.rect.origin.y)];
    glPopMatrix();
}

- (ScrollViewElement*)getCellAtPos:(CGPoint)pos {
    for (ScrollViewElement* element in self.elements) {
        if (![element isKindOfClass:[ScrollViewElement class]]) continue;
        
        if (pointRectCol(CGRectMake(element.rect.origin.x, element.rect.origin.y + refPointY, element.rect.size.width, element.rect.size.height), pos)) {
            return element;
        }
    }
    return nil;
}

@end