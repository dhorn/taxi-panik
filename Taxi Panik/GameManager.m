//
//  GameManager.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameManager.h"
#import "MainView.h"
#import "AppDelegate.h"
#import "Wheel.h"

int w = 480;
int h = 320;
int gameSpeed = SLOW;
NSTimeInterval deltatime = 0.1;
float fps = 30.0;

@implementation GameManager
@synthesize objectsToAdd, speed, state, countDownNumber, level = currentLevel;

#pragma mark -
#pragma mark class Methods
+ (GameManager*)getInstance {
    static GameManager* gameManager;
    if (!gameManager) {
        gameManager = [[GameManager alloc] init];
        [gameManager preloader];
    }
    return gameManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        if (IS_IPHONE5) {
            w = 568;
        }
        gameObjects  = [[NSMutableArray alloc] initWithCapacity:100];
        objectsToAdd = [[NSMutableArray alloc] initWithCapacity:50];
        destroyable  = [[NSMutableArray alloc] initWithCapacity:50];
        //        zoomed = false;
        currentzoom = 1.0;
        timeinterval = 10;  //first speedup ten seconds after start
        increaseDifficultyTimer = timeinterval;
        customerSpawnTimer = 10.0;
        
        playGameCountdown = 0;
        startGame = false;
        countDownNumber = 3;
        
        toggle = true;
        creditsFade = 1.0;
        creditsTimer = 0.0;
        
        easterEgg = false;
        developer = false;
        currentIndex = 0;
        
        //Set Combi, not random generated
        easterEggArray[0] = CGRectMake(0, 0, 32, 32);
        easterEggArray[1] = CGRectMake(w-32, h-32, 32, 32);
        easterEggArray[2] = CGRectMake(w-32, 0, 32, 32);
        easterEggArray[3] = CGRectMake(0, h-32, 32, 32);
        running = false;
        
        levelArray = [[NSArray arrayWithObjects:@"Lvl1", @"Lvl2", @"Lvl3", @"Lvl4", nil] retain];
        //        background = [[Sprite alloc] initWithTexture:@"Background.png"
        //                                                rect:CGRectMake(w/2-64, h/2-64, 128, 128)
        //                                               speed:makeVector(0, 0, 0)
        //                                                  ai:-1
        //                                               angle:0];
        fadeIn = false;
        fadeOut = false;
        fadeAlpha = 1;
        
        costToContinue = 1;
        spawnWheelTimer = 1000;
        
        [self newRandomChunkTypeArray];
    }
    return self;
}

- (void)newRandomChunkTypeArray {
    for (int i = 0; i < sizeof(chunkTypes) / sizeof(int); i++) {
        int random = arc4random() % 10;
        chunkTypes[i] = DECORATION_GRASS;
        if (random == 9) chunkTypes[i] = DECORATION_HOUSE;
    }
}

- (void)preloader {
    [self setOGLProjection];
    state = LOAD_GAME;
    [Font getInstance];
    score = 0;
    spawnWheelTimer = 1000;
    
    //Load Music
    [[SoundManager getInstance] getSound:SOUND_THEME_1];
    [[SoundManager getInstance] getSound:SOUND_THEME_2];
    [[SoundManager getInstance] getSound:SOUND_BUTTON_PRESS];
    [[SoundManager getInstance] getSound:SOUND_EXPLOSION];
    [[SoundManager getInstance] getSound:SOUND_SQUEAL];
    
    //Load sfx
    //    [[TextureManager getInstance] getTextureObject:@"TP_Title.png"];
    
    //Start Audio Theme Playback
    NSString* string = @"";
    [[SoundManager getInstance] generateRandomThemeSongName:&string];
    [[SoundManager getInstance] playMusic:string];

    sfxOn = true;
    musicOn = true;
}

- (void)start {
    running = true;
}

- (void)stop {
    running = false;
    glFinish();
}

- (BOOL)running {
    return running;
}

#pragma mark -
#pragma mark game Methods
- (void)touchesBegan:(NSSet*)touches inView:(UIView*)view { // No Multitouch
    //    UITouch* touch = [touches anyObject];
    CGPoint location = [[touches anyObject] locationInView:view];
    if (IS_IPHONE5) {
        location.x-=IPHONE5_TRANS;
    }
    
    if (rectRectCol(easterEggArray[currentIndex], CGRectMake(location.x, location.y, 3, 3))) {
        currentIndex++;
        if (currentIndex > EASTEREGGCOMBILOCKCOUNT-1) {
            easterEgg = easterEgg ? false : true;
            developer = developer ? false : true;
            currentIndex = 0;
        }
    } else {
        currentIndex = 0;
    }
    [[MenuManager getInstance] touch:[[touches anyObject] locationInView:view]
                              point2:[[touches anyObject] locationInView:view]
                               event:EVENTTOUCHDOWN];
    
    switch (state) {
        case PLAY_GAME: //Game Stuff
            if (paused) { return; }
            for (UITouch* touch in [touches allObjects]) {
                float dist = 32*32;
                Player* closestPlayer = nil;
                GameObject* touchObject = [[GameObject alloc] init];
                if (!IS_IPHONE5) {
                    touchObject.rect = CGRectMake([touch locationInView:view].x, [touch locationInView:view].y, 3.0, 3.0);
                } else {
                    touchObject.rect = CGRectMake([touch locationInView:view].x-IPHONE5_TRANS, [touch locationInView:view].y, 3.0, 3.0);
                }
                
                for (Player* p in gameObjects) {
                    if (![p isKindOfClass:[Player class]]) {
                        if ([p isKindOfClass:[Wheel class]]) {
                            Wheel* wheel = (Wheel*) p;
                            if (rectRectCol(wheel.rect, CGRectMake([touch locationInView:view].x, [touch locationInView:view].y, 10, 10))) {
                                wheel.direction = normalize(makeVector((w-37) - wheel.rect.origin.x, 2 - wheel.rect.origin.y, 0));
                                wheel.clicked = true;
                            }
                        }
                        continue;
                    }
                    if (p.rotate) continue;
                    if (squareDistCCOfRects(p.rect, touchObject.rect) < dist) {
                        dist = squareDistCCOfRects(p.rect, touchObject.rect);
                        closestPlayer = p;
                    }
                }
                
                if (closestPlayer == nil) continue;
                for (Chunk* streetChunk in currentLevel.chunks) {
                    if (![streetChunk isKindOfClass:[StreetChunk class]]) continue;
                    
                    if (pointRectCol(streetChunk.rect, CGPointMake(closestPlayer.rect.origin.x + closestPlayer.rect.size.width/2,
                                                                   closestPlayer.rect.origin.y + closestPlayer.rect.size.height/2))) {
                        [closestPlayer.pathPoints addObject:touch];
                        [closestPlayer.pathPoints addObject:streetChunk];
                        [closestPlayer toggleDisplayOldPathPoints];
                        return;
                        
                    }
                }
            }
            break;
        case GAME_CHOOSE_LEVEL:
            if (developer) {
                state = PLAY_GAME;
                break;
            }
        default:
            break;
    }
}

- (void)touchesMoved:(NSSet*)touches inView:(UIView*)view {
    //touch.timestamp
    switch (state) {
        case PLAY_GAME: //Game Stuff
            // Do Game Stuff before Player Touch Tracking...
        {
            if (paused) { return; }
            //            UITouch* touch = [touches anyObject];
            //            CGPoint p1 = [touch locationInView:view];
            //            CGPoint p2 = [touch previousLocationInView:view];
            //            [[MenuManager getInstance] touch:p1
            //                                      point2:p2
            //                                       event:EVENTTOUCHMOVE];
        }
            for (UITouch* touch in [touches allObjects]) {
                GameObject* touchObject = [[GameObject alloc] init];
                if (!IS_IPHONE5) {
                    touchObject.rect = CGRectMake([touch locationInView:view].x, [touch locationInView:view].y, 3.0, 3.0);
                } else {
                    touchObject.rect = CGRectMake([touch locationInView:view].x-IPHONE5_TRANS, [touch locationInView:view].y, 3.0, 3.0);
                }                //Find Chunk We Touched...
                for (Player* p in gameObjects) {
                    if (![p isKindOfClass:[Player class]]) continue;
                    Chunk *c = p.pathPoints.lastObject;
                    if (squareDistCCOfRects(touchObject.rect, c.rect) < 32 * 32) continue;
                    float dist = 128*128; //50 units away
                    StreetChunk* closestChunk = nil;
                    //Find Chunk thats closer than four Chunks...
                    for (StreetChunk* streetChunk in c.neighboors) {
                        if (![streetChunk isKindOfClass:[StreetChunk class]]) continue;
                        
                        if (squareDistCCOfRects(streetChunk.rect, touchObject.rect) < dist) {
                            dist = squareDistCCOfRects(streetChunk.rect, touchObject.rect);
                            closestChunk = streetChunk;
                        }
                    }
                    if (closestChunk != nil) {
                        BOOL ok = true;
                        for (Chunk* chunk in p.pathPoints) {
                            if (chunk == closestChunk) {
                                ok = false;
                                break;
                            }
                        }
                        if (ok) {
                            if (p.pathPoints.count > 1) {
                                [p.pathPoints addObject:closestChunk];
                            } else {
                                [p.pathPoints addObject:touch];
                                [p.pathPoints addObject:closestChunk];
                            }
                        }
                    }
                }
                
                float dist = 32*32;
                Player* closestPlayer = nil;
                
                for (Player* p in gameObjects) {
                    if (![p isKindOfClass:[Player class]]) continue;
                    if (p.rotate) continue;
                    if (squareDistCCOfRects(p.rect, touchObject.rect) < dist) {
                        dist = squareDistCCOfRects(p.rect, touchObject.rect);
                        closestPlayer = p;
                    }
                }
                for (Player* p in gameObjects) {
                    if (![p isKindOfClass:[Player class]]) continue;
                    
                    if (p.pathPoints.count > 1) {
                        if ([p.pathPoints objectAtIndex:0] == touch) return;
                    }
                }
                
                if (closestPlayer == nil) continue;
                if (closestPlayer.pathPoints.count > 1) continue;
                for (Chunk* streetChunk in currentLevel.chunks) {
                    if (![streetChunk isKindOfClass:[StreetChunk class]]) continue;
                    
                    if (pointRectCol(streetChunk.rect, CGPointMake(closestPlayer.rect.origin.x + closestPlayer.rect.size.width/2,
                                                                   closestPlayer.rect.origin.y + closestPlayer.rect.size.height/2))) {
                        [closestPlayer.pathPoints addObject:touch];
                        [closestPlayer.pathPoints addObject:streetChunk];
                        [closestPlayer toggleDisplayOldPathPoints];
                        return;
                        
                    }
                }
            }
            
            //
            //  NOTE: DO NEVER EVER PLACE ANYTHING BELLOW THIS POINT, WON'T BE EXECUTED AS EXPECTED
            //
            
            break;
        default:
        {
            UITouch* touch = [touches anyObject];
            CGPoint p1 = [touch locationInView:view];
            CGPoint p2 = [touch previousLocationInView:view];
            [[MenuManager getInstance] touch:p1
                                      point2:p2
                                       event:EVENTTOUCHMOVE];
            //            NSLog(@"p1[%f|%f]; p2[%f|%f]", p1.x, p1.y, p2.x, p2.y);
        }
            break;
    }
}

- (void)touchesEnded:(NSSet*)touches inView:(UIView*)view {
    switch (state) {
        case PLAY_GAME: //Game Stuff
        {
            BOOL unpause = false;
            if (paused) {
                unpause = true;
            }
            [[MenuManager getInstance] touch:[[touches anyObject] locationInView:view]
                                      point2:[[touches anyObject] locationInView:view]
                                       event:EVENTTOUCHUP];
            if (unpause)  {
                paused = false;
                [[MenuManager getInstance] destroy];
                [[MenuManager getInstance] addElement:BUTTON
                                                 rect:CGRectMake(2, 2, 32, 32)
                                                angle:0
                                                title:BUTTON_PAUSE];
                return;
            }
        }
            //            if (!startGame) startGame = true;
            for (UITouch* touch in [touches allObjects]) {
                GameObject* object2 = [[GameObject alloc] init];
                if (!IS_IPHONE5) {
                    object2.rect = CGRectMake([touch locationInView:view].x, [touch locationInView:view].y, 3.0, 3.0);
                } else {
                    object2.rect = CGRectMake([touch locationInView:view].x-IPHONE5_TRANS, [touch locationInView:view].y, 3.0, 3.0);
                }                for (Player* player in gameObjects) {
                    if ([player isKindOfClass:[Player class]]) {
                        if ([player.pathPoints count] < 1) { continue; }
                        if ([player.pathPoints objectAtIndex:0] == touch) {
                            player.oldPathPoints = [player.pathPoints mutableCopy];
                            [player.oldPathPoints removeObjectAtIndex:0];   //Remove Touch since it's invalid after this method
                            [player.pathPoints removeAllObjects];
                            [player toggleDisplayOldPathPoints];
                            return;
                        }
                    }
                }
            }
            break;
        case GAME_CREDITS:
            if (creditsFade < 0.5) {
                state = LOAD_OPTIONS;
                creditsFade = 1.0;
                creditsTimer = 0.0;
            }
            break;
        case WON_GAME:
            state = LOAD_GAME;
            [self resetGame];
            break;
        default:
            [[MenuManager getInstance] touch:[[touches anyObject] locationInView:view]
                                      point2:[[touches anyObject] locationInView:view]
                                       event:EVENTTOUCHUP];
            break;
    }
}

- (void)touchesCancelled:(NSSet*)touches inView:(UIView*)view {
    switch (state) {
        case PLAY_GAME: //Game Stuff
        {
            BOOL unpause = false;
            if (paused) {
                unpause = true;
            }
            [[MenuManager getInstance] touch:[[touches anyObject] locationInView:view]
                                      point2:[[touches anyObject] locationInView:view]
                                       event:EVENTTOUCHUP];
            if (unpause)  {
                paused = false;
                [[MenuManager getInstance] destroy];
                [[MenuManager getInstance] addElement:BUTTON
                                                 rect:CGRectMake(2, 2, 32, 32)
                                                angle:0
                                                title:BUTTON_PAUSE];
                return;
            }
        }
            for (UITouch* touch in [touches allObjects]) {
                GameObject* object2 = [[GameObject alloc] init];
                if (!IS_IPHONE5) {
                    object2.rect = CGRectMake([touch locationInView:view].x, [touch locationInView:view].y, 3.0, 3.0);
                } else {
                    object2.rect = CGRectMake([touch locationInView:view].x-IPHONE5_TRANS, [touch locationInView:view].y, 3.0, 3.0);
                }
                for (Player* player in gameObjects) {
                    if ([player isKindOfClass:[Player class]]) {
                        if ([player.pathPoints count] < 1) { continue; }
                        if ([player.pathPoints objectAtIndex:0] == touch) {
                            player.oldPathPoints = [player.pathPoints mutableCopy];
                            [player.oldPathPoints removeObjectAtIndex:0];   //Remove Touch since it's invalid after this method
                            [player.pathPoints removeAllObjects];
                            [player toggleDisplayOldPathPoints];
                            return;
                        }
                    }
                }
            }
            
            break;
        default:
            [[MenuManager getInstance] touch:[[touches anyObject] locationInView:view]
                                      point2:[[touches anyObject] locationInView:view]
                                       event:EVENTTOUCHUP];
            break;
    }
}

- (void)acceleration:(UIAcceleration*)accel {
    accelX += (accel.x - 0.4)*-20;
    accelY += (accel.y)*-20;
    if (accelX < -h) accelX = -h;
    if (!IS_IPHONE5) {
        if (accelY < -w) accelY = -w;
    } else {
        if (accelY < -390) accelY = -390;
    }
    if (accelX > 0) accelX = 0;
    if (accelY > 0) accelY = 0;
    
    //    NSLog(@"X: %f | Y: %f", accel.x, accel.y);
}

- (void)playGame {
    if (!paused) {
        spawnWheelTimer --;
        if (spawnWheelTimer <= 0) {
            spawnWheelTimer = (arc4random() % 501) + 500;   /* minumum timeout = 500 ticks = 16.6 seconds, max = 33.3 seconds => Average = 25s */
            /* spawn a wheel */
            Wheel* wheel = [[Wheel alloc] initWithTexture:@"wheel.png"
                                                     rect:CGRectMake(32 + (arc4random() % w-64), -50, 32, 32)
                                                    speed:makeVector(0, (arc4random() % 100) + 150, 0)
                                                       ai:0
                                                    angle:0];
            [objectsToAdd addObject:wheel];
        }
        
        timer++; if (timer > 9999999) { timer = 0; }
        if (timer % 4 == 0) {
            score += carsThatCount;
        }
        carsInGame = 0;
        carsThatCount = 0;
        hasCar = false;
        if (!SPAMWITHDACUSTOMA) {
            customerSpawnTimer += deltatime;
            if (customerSpawnTimer >= 10.0) {    //Every 10.0 seconds we spawn a customer
                [self spawnCustomer];
                customerSpawnTimer -= 10.0;
            }
        } else [self spawnCustomer];
        
        increaseDifficultyTimer += deltatime;
        if (increaseDifficultyTimer >= timeinterval) {  //Note: on init set this timer to 60 !!!
            [self spawnEntityOnRandomTick]; score += timer == 1 ? 0 : SPEEDUP;
            increaseDifficultyTimer -= timeinterval;
            timeinterval += 3;  //Add 3 seconds each time
        }
    }
    if (!paused) {
        if (state == WON_GAME) { return; }
        [self manageSprites];
    }
    
    if (IS_IPHONE5) {
        DecorationChunk* chunk = [[DecorationChunk alloc] init];        
        chunk.type = chunkTypes[0];
        int chunkNum = 0;
        for (int x = 0; x <= 524; x+=524) {
            for (int y = 0; y < 320; y+=44) {
                chunk.type = chunkTypes[chunkNum];
                chunkNum++;
                chunk.rect = CGRectMake(x, y, 44, 44);
                [chunk render];
            }
        }
    }
    glPushMatrix();
    if (IS_IPHONE5) {
        glTranslatef(IPHONE5_TRANS, 0, 0);
    }
    [currentLevel render];

    [self drawOGLRect:CGRectMake(0, 0, w, 40) color:makeVector(0, 0, 0) alpha:0.5];

    [self renderSprites];
    
    if (!paused) {
        [[Font getInstance] drawString:[NSString stringWithFormat:NSLocalizedString(@"YOUR_SCORE", nil), score] at:CGPointMake(w/2-75, 6)];
        if (!hasCar) {
            if (currentLevel.numPlayers > 0)
                [self spawnEntityOnRandomTick];
            else {
                /* ask user if he whishes to continue */
                if (steeringWheels >= costToContinue) {
                    [((AppDelegate*)[[UIApplication sharedApplication] delegate]) stopLoop];
                    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LOST", nil)
                                                message:[NSString stringWithFormat:NSLocalizedString(@"DOWISHTOCONTINUE", nil), costToContinue]
                                               delegate:self
                                      cancelButtonTitle:NSLocalizedString(@"NO", nil)
                                      otherButtonTitles:NSLocalizedString(@"YES", nil), nil] show];
                } else {
                    [self alertView:nil clickedButtonAtIndex:0];
                }
            }
        }
    } else {
        if (IS_IPHONE5) {
            glTranslatef(-IPHONE5_TRANS, 0, 0);
        }
        [self drawOGLRect:CGRectMake(0, 0, w, h) color:makeVector(0, 0, 0) alpha:0.7];
        NSString* pString = NSLocalizedString(@"PAUSED", nil);
        NSString* trString = NSLocalizedString(@"TAP_RESUME", nil);
        float pLength = pString.length/2;
        float trLength = trString.length/2;
        [[Font getInstance] drawString:pString at:CGPointMake(w/2-pLength*(WIDTH/3), h/2-25)];
        [[Font getInstance] drawString:trString at:CGPointMake(w/2-trLength*(WIDTH/3), h/2+5)];
    }
    glPopMatrix();
}


- (void)spawnEntityOnRandomTick {
    [[SoundManager getInstance] playSound:@"SpeedUp.wav"];
    if (toggle) {
        toggle = false;
        if (currentLevel.numPlayers > 0) {
            currentLevel.numPlayers--;
            int numStreets = [[currentLevel.chunkCounts objectForKey:NUMSTREETCHUNKS] intValue];
            //Find Random Street Chunk to Spawn
            for (StreetChunk* chunk in currentLevel.chunks) {
                if ([chunk isKindOfClass:[StreetChunk class]]) {
                    numStreets--;
                    //                    int ran = (arc4random() % numStreets-1)+1;
                    int ran = (arc4random() % numStreets)+1;
                    if ((ran == 1 && chunk.type == STREET_STRAIGHT) || numStreets == 0) {
                        //                        NSLog(@"chunk.pos:\n%f\n%f", chunk.pos.x, chunk.pos.y);
                        Player* player = [[Player alloc] initWithTexture:@"car_animated.png"
                                                                    rect:CGRectMake(chunk.rect.origin.x + 2,
                                                                                    chunk.rect.origin.y + 2,
                                                                                    28.0,   //Width
                                                                                    28.0)   //Height
                                                                   speed:makeVector(0.0, 0.0, 0.0)
                                                                      ai:AIDEFAULT
                                                                   angle:chunk.angle];
                        //                        NSLog(@"Angle: %f", chunk.angle);
                        [self.objectsToAdd addObject:player];
                        return;
                    }
                }
            }
        } else {
//            int numStreets = [[currentLevel.chunkCounts objectForKey:NUMSTREETCHUNKS] intValue];
//            //Find Random Street Chunk to Spawn
//            for (StreetChunk* chunk in currentLevel.chunks) {
//                if ([chunk isKindOfClass:[StreetChunk class]]) {
//                    numStreets--;
//                    //                    int ran = (arc4random() % numStreets-1)+1;
//                    int ran = (arc4random() % numStreets)+1;
//                    if ((ran == 1 && chunk.type == STREET_STRAIGHT) || numStreets == 0) {
//                        //                        NSLog(@"chunk.pos:\n%f\n%f", chunk.pos.x, chunk.pos.y);
//                        Enemy* enemy = [[Enemy alloc] initWithTexture:@"NoTexture.png"
//                                                                 rect:CGRectMake(chunk.rect.origin.x + 2,
//                                                                                 chunk.rect.origin.y + 2,
//                                                                                 28.0,   //Width
//                                                                                 28.0)   //Height
//                                                                speed:makeVector(0.0, 0.0, 0.0)
//                                                                   ai:AIDEFAULT
//                                                                angle:chunk.angle];
//                        //                        NSLog(@"Angle: %f", chunk.angle);
//                        for (Customer* customer in gameObjects) {
//                            if ([customer isKindOfClass:[Customer class]]) {
//                                [enemy findPathTo:customer.rect.origin];
//                                break;
//                            }
//                        }
//                        [self.objectsToAdd addObject:enemy];
//                        NSLog(@"spawned Enemy");
//                        return;
//                    }
//                }
//            }
        }
    } else {
        toggle = true;
        switch (gameSpeed) {
            case SLOW:
                gameSpeed = MEDIUM;
                break;
            case MEDIUM:
                gameSpeed = FAST;
                break;
            case FAST:
                gameSpeed = INSANE;
                break;
            case INSANE:
                /* Continue infinitely until all the player's cars died... */
                //                /* Perhaps Won? => for now, go back to Menu*/
                //                state = WON_GAME;
                return;
            default:
                NSLog(@"Unknown GameSpeed");
                break;
        }
    }
}

- (void)spawnCustomer {
    int numStreets = [[currentLevel.chunkCounts objectForKey:NUMSTREETCHUNKS] intValue];
    for (StreetChunk* chunk in currentLevel.chunks) {
        if ([chunk isKindOfClass:[StreetChunk class]]) {
            numStreets--;
            int ran;
            if (numStreets <= 1) ran = 1; else ran = (arc4random() % numStreets-1)+1;
            if (ran == 1) {
                int rndX = (arc4random() % 17) + 1, rndY = (arc4random() % 17) + 1;
                [objectsToAdd addObject:[[Customer alloc] initWithTexture:@"Customer.png"
                                                                     rect:CGRectMake(chunk.rect.origin.x + rndX, chunk.rect.origin.y + rndY, 15, 15)
                                                                    speed:makeVector(0, 0, 0)
                                                                       ai:-1
                                                                    angle:0]];
                return;
            }
        }
    }
}

- (void)toggleStates {
    switch (state) {
        case LOAD_GAME:
            state = START_GAME;
            break;
        case START_GAME:
            state = GAME_LOAD_CHOOSE_LEVEL;
            break;
        case PLAY_GAME:
            state = LOAD_WON_GAME;
            break;
        case LOAD_OPTIONS:
            state = GAME_OPTIONS;
            break;
        case LOAD_CREDITS:
            state = GAME_CREDITS;
        default:
            break;
    }
}

- (void)buttonEvent:(NSString*)bt {
    if ([bt isEqualToString:BUTTON_START]) {
        [self toggleStates];
        fadeIn = true;
    } else if ([bt isEqualToString:BUTTON_OPTIONS]) {
        state = LOAD_OPTIONS;
        //        fadeIn = true;
    } else if ([bt isEqualToString:BUTTON_CREDITS]) {
        state = LOAD_CREDITS;
        fadeIn = true;
    } else if ([bt isEqualToString:BUTTON_MUSIC_ON]) {
        musicOn = false;
        [self setupOptions];
        [[SoundManager getInstance] pauseMusic];
    } else if ([bt isEqualToString:BUTTON_MUSIC_OFF]) {
        musicOn = true;
        [self setupOptions];
        [[SoundManager getInstance] unPauseMusic];
    } else if ([bt isEqualToString:BUTTON_SFX_ON]) {
        sfxOn = false;
        [self setupOptions];
    } else if ([bt isEqualToString:BUTTON_SFX_OFF]) {
        sfxOn = true;
        [self setupOptions];
    } else if ([bt isEqualToString:BUTTON_MENU]) {
        /* reset Game */
        [self resetGame];
        state = LOAD_GAME;
        fadeIn = true;
    } else if ([bt isEqualToString:BUTTON_BACK]) {
        if (state == GAME_INAPP) {
            state = LOAD_STORE;
        } else {
            /* reset Game */
            [self resetGame];
        
            state = LOAD_GAME;
            fadeIn = true;
        }
    } else if ([bt isEqualToString:BUTTON_SCORE]) {
        if (((AppDelegate*)[[UIApplication sharedApplication] delegate]).gameCenterEnabled) {
            [((AppDelegate*)[[UIApplication sharedApplication] delegate]) stopLoop];
            GKGameCenterViewController* controller = [[GKGameCenterViewController alloc] init];
            controller.gameCenterDelegate = self;
//        controller.viewState = GKGameCenterViewControllerStateLeaderboards;
//        controller.leaderboardCategory = @"com.taxipanik.leaderboards.highscore";
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentModalViewController:controller
                                                                                                          animated:YES];
        } else {
            fadeIn = true;
            state = LOAD_SCORE;
        }
    } else if ([bt isEqualToString:BUTTON_PAUSE]) {
        [self pause];
    } else if ([bt isEqualToString:BUTTON_STORE]) {
//        state = LOAD_INAPP;
        state = LOAD_STORE;
    } else if ([bt isEqualToString:BUTTON_INAPP]) {
        state = LOAD_INAPP;
    }
}

#pragma mark -
#pragma mark drawing Methods
- (void)drawStatesWithFrame:(CGRect)frame {
    if (!running) return;
    [self fps];
    
    if (fadeIn) {
        if (fadeAlpha == 2) fadeAlpha = 1;
        fadeAlpha -= deltatime * 1.0 / 0.5; //Amount of Seconds the Fading takes. (In this one 0.5 Seconds)
        if (fadeAlpha <= 0) {
            fadeIn = false;
            fadeAlpha = 2;
        }
    }
    
    if (fadeOut) {
        if (fadeAlpha == 2) fadeAlpha = 0;
        fadeAlpha += deltatime * 1.0 / 0.5; //Amount of Seconds the Fading takes. (0.5 Seconds)
        if (fadeAlpha >= 1) {
            fadeOut = false;
            fadeAlpha = 2;
        }
    }
    
    switch (state) {
        case LOAD_GAME:
            //Cleanup our previous Buttons
            [self loadLevel:[levelArray objectAtIndex:1]];
            [[MenuManager getInstance] destroy];
            [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w/2-50.0, 60.0, 100.0, 50.0)  angle:0.0 title:BUTTON_START];
            [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w/2-50.0, 125.0, 100.0, 50.0) angle:0.0 title:BUTTON_OPTIONS];
            [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w/2-50.0, 190.0, 100.0, 50.0) angle:0.0 title:BUTTON_SCORE];
            [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w-220, h-60, 210.0, 50.0) angle:0 title:BUTTON_INAPP];
            [self toggleStates];
            fadeIn = true;
            break;
        case START_GAME:
            //Render Menu
            [self renderAnimatedBackground];
            [[MenuManager getInstance] render];
            Sprite* s = [[[Sprite alloc] initWithTexture:@"TP_Title.png" rect:CGRectMake(w/2-100, -10, 200, 100) speed:makeVector(0, 0, 0) ai:0 angle:0] autorelease];
            [s render];
            break;
        case GAME_LOAD_CHOOSE_LEVEL:
            [self renderAnimatedBackground];
            [[MenuManager getInstance] destroy];
            [[MenuManager getInstance] addElement:SCROLLVIEW rect:CGRectMake(0, 0, w, h-60) angle:0 title:@""];
            //            [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(2, h-52, 100, 50) angle:0 title:BUTTON_BACK];
            state = GAME_CHOOSE_LEVEL;
            fadeIn = true;
            break;
        case GAME_CHOOSE_LEVEL:
            [self renderAnimatedBackground];
            [[MenuManager getInstance] render];
            break;
        case PLAY_GAME:
            //            if (startGame) {
            
            [self playGame];
            [[MenuManager getInstance] render];
            //            } else {
            //                [[Font getInstance] drawString:@"Tap Screen to Start Playing" at:CGPointMake(0, 0)];
            //            }
            
            break;
        case LOAD_WON_GAME:
            [[MenuManager getInstance] destroy];
            state = WON_GAME;
            break;
        case WON_GAME:
            [self renderAnimatedBackground];
            //            [self drawOGLRect:CGRectMake(0, 0, w, h) color:makeVector(0, 0, 0) alpha:0.7];
            float s1 = NSLocalizedString(@"CONGRATULATIONS", nil).length/2;
            float s2 = NSLocalizedString(@"TAP_CONTINUE", nil).length/2;
            
            [[Font getInstance] drawString:[NSString stringWithFormat:NSLocalizedString(@"CONGRATULATIONS", nil), score] at:CGPointMake(w/2-s1*(WIDTH/3), h/2-40)];
            [[Font getInstance] drawString:NSLocalizedString(@"TAP_CONTINUE", nil) at:CGPointMake(w/2-s2*(WIDTH/3), h/2)];
            break;
        case LOAD_CREDITS:
            //Cleanup our previous Buttons
            [self renderAnimatedBackground];
            [[MenuManager getInstance] destroy];
            
            [self toggleStates];
            break;
        case GAME_CREDITS:
            if (creditsFade > 0) {
                creditsFade -= deltatime*(1.0/10);
                if (creditsFade < 0) creditsFade = 0.0;
            } else {
                creditsTimer += deltatime;
            }
            NSString* string1 = NSLocalizedString(@"CREDITS_1", nil);
            NSString* string2 = NSLocalizedString(@"CREDITS_2", nil);
            NSString* string3 = NSLocalizedString(@"CREDITS_3", nil);
            float l1 = string1.length/2;
            float l2 = string2.length/2;
            float l3 = string3.length/2;
            [[Font getInstance] drawString:string1 at:CGPointMake(w/2-l1*(WIDTH/3), h/2-40)];
            [[Font getInstance] drawString:string2 at:CGPointMake(w/2-l2*(WIDTH/3), h/2-20)];
            [[Font getInstance] drawString:string3 at:CGPointMake(w/2-l3*(WIDTH/3), h/2)];
            [self drawOGLRect:CGRectMake(0, 0, w, h) color:makeVector(0, 0, 0) alpha:creditsFade];
            break;
        case LOAD_OPTIONS:
            //Cleanup our previous Buttons
            [self setupOptions];
            [self toggleStates];
            fadeIn = true;
            break;
        case GAME_OPTIONS:
            [self renderAnimatedBackground];
            Sprite* sprite = [[[Sprite alloc] initWithTexture:@"TP_Options.png" rect:CGRectMake(w/2-100, -10, 200, 100) speed:makeVector(0, 0, 0) ai:0 angle:0] autorelease];
            [sprite render];
            [[MenuManager getInstance] render];
            break;
        case LOAD_SCORE:
            //Load Score from File
            [self renderAnimatedBackground];
            [[MenuManager getInstance] destroy];
            
            [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(20, h-70, 100, 50)
                                            angle:0
                                            title:BUTTON_BACK];
            state = GAME_SCORE;
            break;
        case GAME_SCORE:
            //Display Score + Back button
            [self renderAnimatedBackground];
            [[MenuManager getInstance] render];
            
            int y = 20, x = 20, placement = 1;
            for (NSNumber* num in scores) {
                [[Font getInstance] drawString:[NSString stringWithFormat:NSLocalizedString(@"SCORE_ON_P", nil), placement, num] at:CGPointMake(x, y)];
                placement++;
                y+=30;
                if (y > h-100) { y = 20; x += w/2-20; }
            }
            break;
        case LOAD_STORE:
            [[MenuManager getInstance] destroy];
            [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w-260, h-60, 250, 50) angle:0 title:BUTTON_INAPP];
            [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(10, h-60, 100, 50) angle:0 title:BUTTON_MENU];
            state = GAME_STORE;
            fadeIn = true;
            break;
        case GAME_STORE:
            [self renderAnimatedBackground];
            [[MenuManager getInstance] render];
            break;
        case LOAD_INAPP:
            [[MenuManager getInstance] destroy];
            [[MenuManager getInstance] addElement:INAPP rect:CGRectMake(0, 0, 0, 0) angle:0 title:@""];
            [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(10, h-60, 100, 50)
                                            angle:0
                                            title:BUTTON_MENU];
            
            state = GAME_INAPP;
            fadeIn = true;
            break;
        case GAME_INAPP:
            [self renderAnimatedBackground];
            [[MenuManager getInstance] render];
            break;
        default:
            NSLog(@"Unknown Gamestate: %i", state);
            break;
    }
    
    if (fadeAlpha != 2) {
        [self renderFade];
    }
    if (developer) {
        [[Font getInstance] drawString:[NSString stringWithFormat:@"FPS: %i", fps] at:CGPointMake(32.0, 30.0)];
        [[Font getInstance] drawString:[NSString stringWithFormat:@"Entity: %i", gameObjects.count] at:CGPointMake(32.0, 50.0)];
        [[Font getInstance] drawString:[NSString stringWithFormat:@"Chunk: %i", currentLevel.chunks.count] at:CGPointMake(32.0, 70.0)];
        [[Font getInstance] drawString:[NSString stringWithFormat:@"New Wheel in: %.1f", (float)spawnWheelTimer / 30.0]
                                    at:CGPointMake(32.0, 90.0)];
        [[Font getInstance] drawString:[NSString stringWithFormat:@"Spawn Customer in: %is", (int) (11.0 - customerSpawnTimer)]
                                    at:CGPointMake(225.0, 30.0)];
        [[Font getInstance] drawString:[NSString stringWithFormat:@"Difficulty upping in: %is", (int) (timeinterval+1 - increaseDifficultyTimer)]
                                    at:CGPointMake(225.0, 50.0)];
    }
    Sprite* tmp = [[Sprite alloc] initWithTexture:@"SteeringWheel.png" rect:CGRectMake(w-37, 2, 64, 32) speed:makeVector(0, 0, 0) ai:0 angle:0];
    [tmp render];
    int letters = ((NSString*)[NSString stringWithFormat:@"%i", steeringWheels]).length;
    letters*=10;
    [[Font getInstance] drawColoredString:[NSString stringWithFormat:@"%i", steeringWheels] at:CGPointMake(w-(40+letters), 6) color:makeVector(255, 255,255)];
}

- (void)drawOGLLinefrom:(CGPoint)p1 to:(CGPoint)p2 color:(Vector)vec alpha:(float)alpha {
    glDisable(GL_TEXTURE_2D);
    glLineWidth(3.0);
    GLshort vertices [] = {
        p1.x+1.0, p1.y+1.0,
        p2.x+1.0, p2.y+1.0
    };
    glColor4f(vec.x, vec.y, vec.z, alpha);
    glVertexPointer(2, GL_SHORT, 0, &vertices);
    glDrawArrays(GL_LINES, 0, 2);
    glEnable(GL_TEXTURE_2D);
    glColor4f(1, 1, 1, 1);
}

- (void)drawOGLRect:(CGRect)rect color:(Vector)v alpha:(float)a {
    glDisable(GL_TEXTURE_2D);
    GLshort vertices [] = {
        0.0, rect.size.height,
        rect.size.width, rect.size.height,
        0.0, 0.0,
        rect.size.width, 0.0,
    };
    
    glColor4f(v.x, v.y, v.z, a);
    glVertexPointer(2, GL_SHORT, 0, &vertices);
    glPushMatrix();
    
    glTranslatef(rect.origin.x, rect.origin.y, 0.0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glPopMatrix();
    glEnable(GL_TEXTURE_2D);
    glColor4f(1, 1, 1, 1);
}

#pragma mark -
#pragma mark helper Methods

- (void)scrollViewClick:(int)index {
    if (index > levelArray.count-1) return;
    [self loadLevel:[levelArray objectAtIndex:index]];
    if (currentLevel) {
        [[MenuManager getInstance] destroy];
        [[MenuManager getInstance] addElement:BUTTON
                                         rect:CGRectMake(2, 2, 32, 32)
                                        angle:0
                                        title:BUTTON_PAUSE];
        state = PLAY_GAME;
        paused = false;
        fadeIn = true;
    }
}

- (int)levelCount {
    return levelArray.count;
}

- (NSString*)nameForIndex:(int)index {
    switch (index) {
        case 0:
            return NSLocalizedString(@"LEVEL_1", nil);
        case 1:
            return NSLocalizedString(@"LEVEL_2", nil);
        case 2:
            return NSLocalizedString(@"LEVEL_3", nil);
        case 3:
            return NSLocalizedString(@"LEVEL_4", nil);
        default:
            return @"LOL!EPIC!FAIL";
    }
    return @"LOL!EPIC!FAIL";
}


- (void)renderSprites {
    for (Sprite* sprite in gameObjects) {
        [sprite render];
    }
}

- (void)manageSprites {
    for (Sprite* sprite in destroyable) {
        for (Sprite* sprite2 in gameObjects) {
            if (sprite == sprite2) {
                [gameObjects removeObject:sprite2];
                break;
            }
        }
    }
    for (Sprite* sprite in objectsToAdd) {
        [gameObjects addObject:sprite];
    }
    [objectsToAdd removeAllObjects];
    [destroyable removeAllObjects];
    
    
    for (Sprite* sprite in gameObjects) {
        if (sprite && [sprite isKindOfClass:[Sprite class]]) {
            //update Uses to set Animations, particles, sounds, etc
            [sprite update];
            if (!sprite.isActive) { [destroyable addObject:sprite]; if ([sprite isKindOfClass:[Customer class]]) score += CUSTOMER_ANGRY; continue; }
            [self manageCollision:sprite];
            
            if ([sprite isKindOfClass:[Player class]]) {
                hasCar = true;
                carsInGame++;
                if (!((Player*)sprite).displayOldPathPoints) {  /* while a player is displaying it's old pathpoints it doesn't move and should not count */
                    carsThatCount++;
                }
            }
        }
    }
}

- (void)manageCollision:(GameObject*)object {
    BOOL hasMoved = false;
    
    if ([object isKindOfClass:[Player class]]) {
        for (Sprite* c in gameObjects) {
            if ([c isKindOfClass:[Customer class]] && !((Customer*)c).angry) {
                int noticeRadius = 75;
                int mult = 50;
                if (SPAMWITHDACUSTOMA) { noticeRadius = 500; mult = 50; }
                if (squareDistCCOfRects(object.rect, c.rect) < noticeRadius*noticeRadius) {
                    c.speed = makeVector((object.rect.origin.x + object.rect.size.width/2)-(c.rect.origin.x + c.rect.size.width/2),
                                         (object.rect.origin.y + object.rect.size.height/2)-(c.rect.origin.y + c.rect.size.height/2),
                                         0.0);
                    c.speed = normalize(c.speed);
                    c.speed = makeVector(c.speed.x*mult, c.speed.y*mult, c.speed.z*mult);
                }
                //Move to initializer
                [c setRadius:5];
                [object setRadius:16];
                if (sphereCol(object, c)/*aabbCol(object, c)*/) {
                    [destroyable addObject:c];
                    score += CUSTOMER_SERVED;
                }
            }
            if ([c isKindOfClass:[Player class]] && c != object && ((Player*)c).initAppearTimer <= 0) {
                [object setRadius:16];
                [c setRadius:16];
                if (sphereCol(object, c)) {
                    [destroyable addObject:c];
                    [destroyable addObject:object];
                    [objectsToAdd addObject:[[Animation alloc] initWithTexture:@"explosion_8f"
                                                                          rect:CGRectMake(c.rect.origin.x,
                                                                                          c.rect.origin.y, 32, 32)
                                                                         speed:makeVector(0, 0, 0)
                                                                            ai:0
                                                                         angle:0]];
                    score += PLAYERDEAD*2;  /* Two Cars are gone */
                    //Add Explosion Effect...
                    
                    //Explosion Sound
                    [[SoundManager getInstance] playSound:SOUND_EXPLOSION];
                }
            }
        }
        
        //        Player* p = [[Player alloc] init];
        //        for (Chunk* chunk in currentLevel.chunks) {
        //            for (Player* player in gameObjects) {
        //                if (![player isKindOfClass:[Player class]]) { continue; }
        //                if (chunk.type == DECORATION_GRASS && aabbCol(chunk, player)) {
        //                    if (p) {
        //                        p = player;
        //                    }
        //                }
        //                if ([chunk isKindOfClass:[StreetChunk class]] && aabbCol(chunk, player)) {
        //                    p = player;
        //                    break;
        //                }
        //            }
        //        }
        //        if (p) {
        //            [destroyable addObject:p];
        //            score += PLAYERDEAD;
        //            //Explosion
        //
        //        }
        BOOL kill = true;
        for (Chunk* chunk in currentLevel.chunks) {
            [object setRadius:16];
            [chunk setRadius:16];
            if ([chunk isKindOfClass:[StreetChunk class]] && sphereCol(object, chunk) && ((StreetChunk*)chunk).blocked) {
                kill = true;
                break;
            }
            if ([chunk isKindOfClass:[StreetChunk class]] && sphereCol(object, chunk)) {
                kill = false;
                break;
            }
        }
        if (kill) {
            [destroyable addObject:object];
            [objectsToAdd addObject:[[Animation alloc] initWithTexture:@"explosion_8f"
                                                                  rect:CGRectMake(object.rect.origin.x,
                                                                                  object.rect.origin.y, 32, 32)
                                                                 speed:makeVector(0, 0, 0)
                                                                    ai:0
                                                                 angle:0]];
            score += PLAYERDEAD;
            
            //Add Explosion Effect
            
            //Explosion Sound
            [[SoundManager getInstance] playSound:SOUND_EXPLOSION];
        }
    }
    if ([object isKindOfClass:[Customer class]]) {
        bool moveX = true, moveY = true;
        //        GameObject* go = [object copy];
        //        go.rect = CGRectMake(object.rect.origin.x, object.rect.origin.y, 10, 10);
        for (Chunk* chunk in currentLevel.chunks) {
            if (!(chunk.type == DECORATION_HOUSE)) continue;
            GameObject* tmp = [[GameObject alloc] init];
            //NOT FIXED YET!!!! ERRORS AWAITED!!!
            tmp.rect = CGRectMake(object.rect.origin.x + 4, object.rect.origin.y + 4, 7, 7);
            
            if (aabbCol(chunk, tmp)) {
                for (int i = 0; i < 50; i++) {   //three attempts to get the customer unstuck..
                    if (aabbCol(chunk, object)) {
                        object.rect = CGRectMake(object.rect.origin.x+1.0*deltatime,
                                                 object.rect.origin.y+1.0*deltatime,
                                                 object.rect.size.width,
                                                 object.rect.size.height);
                    }
                }
            } else {
                CGRect backup = tmp.rect;
                tmp.rect = CGRectMake(tmp.rect.origin.x+object.speed.x*deltatime,
                                      tmp.rect.origin.y,
                                      tmp.rect.size.width,
                                      tmp.rect.size.height);
                if (aabbCol(chunk, tmp)) moveX = false;
                tmp.rect = backup;
                tmp.rect = CGRectMake(tmp.rect.origin.x,
                                      tmp.rect.origin.y+object.speed.y*deltatime,
                                      tmp.rect.size.width,
                                      tmp.rect.size.height);
                if (aabbCol(chunk, tmp)) moveY = false;
            }
        }
        
        if (moveX) object.rect = CGRectMake(object.rect.origin.x+object.speed.x*deltatime,
                                            object.rect.origin.y,
                                            object.rect.size.width,
                                            object.rect.size.height);
        if (moveY) object.rect = CGRectMake(object.rect.origin.x,
                                            object.rect.origin.y+object.speed.y*deltatime,
                                            object.rect.size.width,
                                            object.rect.size.height);
        hasMoved = true;
    }
    if (hasMoved) return;
    [object move];
}

- (void)setOGLProjection {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(0.0, w, h, 0.0, 0.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnableClientState(GL_VERTEX_ARRAY);
    glDisable(GL_DEPTH_TEST); //2D only
    glEnable(GL_CULL_FACE); //Only Front is renderd
    glEnable(GL_TEXTURE_2D);
    glClearColor(0, 0, 0, 1);
    glEnable(GL_BLEND);
}

- (void)loadLevel:(NSString*)name {
    [currentLevel release];
    currentLevel = [[[LevelLoader alloc] init] loadLevel:name];
}

- (void)fps {
    if (!previous) { previous = [[NSDate date] retain]; deltatime = 0.016; return; }
    deltatime = [[NSDate date] timeIntervalSinceDate:previous];
    [previous release];
    previous = [[NSDate date] retain];
    time+=deltatime; fpscounter++;
    if (time >= 1.0) { time -= 1.0; fps = fpscounter; fpscounter = 0; }
    if (deltatime > 0.5) {      //Have it set to 0 when we are below 2 FPS since that usually means the Player Paused the Game;
        deltatime = 0.0;        //=> If we don't do that all the objects which use deltatime would jump/dissapear
    }
}

- (void)renderAnimatedBackground {
    [accelTimeOut invalidate];
    [accelTimeOut release];
    accelTimeOut = nil;
    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:1.0/30.0];
    accelTimeOut = [[NSTimer scheduledTimerWithTimeInterval:5   //Wait 5 Seconds before timing out accel
                                                     target:self
                                                   selector:@selector(timeOutAccel)
                                                   userInfo:nil
                                                    repeats:false] retain];
    glPushMatrix();
    glTranslatef(accelY, accelX, 0);
    glScalef(2, 2, 1);
    [currentLevel render];
    glPopMatrix();
    [self drawOGLRect:CGRectMake(0, 0, w, h) color:makeVector(0, 0, 0) alpha:0.5];
}

- (void)timeOutAccel {
    [accelTimeOut release];
    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:0];
    accelTimeOut = nil;
}

- (void)zoom:(float)factor {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(0.0, w/factor, h/factor, 0.0, 0.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    //    glLoadIdentity();
}

- (void)animatedZoom:(float)ztv {
    if (!zoom) {    //No Zooming going on at the moment;
        ztimetracker = ztv;
        zoom = [NSTimer scheduledTimerWithTimeInterval:1.0/30.0
                                                target:self
                                              selector:@selector(zoom:)
                                              userInfo:nil
                                               repeats:YES];
        if (ztv > 0.0) { ztv += 0.1; }
        else { ztv -= 0.1; }
    }
    if (ztimetracker > currentzoom) {
        if (ztimetracker - currentzoom < 0.1) {
            [zoom invalidate];
            zoom = nil;
            return;
        }
        currentzoom += (ztimetracker-currentzoom)/100;
    } else {
        if (currentzoom - ztimetracker < 0.1) {
            [zoom invalidate];
            zoom = nil;
            return;
        }
        currentzoom -= (currentzoom-ztimetracker)/100;
    }
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(0.0, w/currentzoom, h/currentzoom, 0.0, 0.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

- (void)setupOptions {
    [[MenuManager getInstance] destroy];
    if (musicOn) [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w/2-50.0, 70,  100.0, 50.0) angle:0.0 title:BUTTON_MUSIC_ON];
    else [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w/2-50.0, 70,  100.0, 50.0) angle:0.0 title:BUTTON_MUSIC_OFF];
    
    if (sfxOn) [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w/2-50.0, 135,  100.0, 50.0) angle:0.0 title:BUTTON_SFX_ON];
    else [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w/2-50.0, 135,  100.0, 50.0) angle:0.0 title:BUTTON_SFX_OFF];
    
    [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w/2-50, 200, 100.0, 50.0) angle:0.0 title:BUTTON_CREDITS];
    [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(w/2-230, 260, 100.0, 50.0) angle:0.0 title:BUTTON_MENU];

    [self saveData];
}

- (BOOL)isMusicOn {
    return true;
}

- (BOOL)isSfxOn {
    return sfxOn;
}

- (void) saveObject: (id) object key: (NSString *) key {
    NSUserDefaults *storage = [NSUserDefaults standardUserDefaults];
    [storage setObject: object forKey: key];
}

- (id) readObjectforKey: (NSString *) key {
    NSUserDefaults *storage = [NSUserDefaults standardUserDefaults];
    return [storage objectForKey: key];
}

- (void)saveData {
    [self saveObject:[NSKeyedArchiver archivedDataWithRootObject:scores] key:KEY_SCORES];
    [self saveObject:[NSKeyedArchiver archivedDataWithRootObject:[NSNumber numberWithBool:musicOn]] key:KEY_MUSIC];
    [self saveObject:[NSKeyedArchiver archivedDataWithRootObject:[NSNumber numberWithBool:sfxOn]] key:KEY_SOUND];
    [self saveObject:[NSKeyedArchiver archivedDataWithRootObject:[NSNumber numberWithInt:steeringWheels]] key:KEY_CURRENCY];
    //    [scores release];
    //    scores = nil;
}

- (void)loadData {
    scores = [NSKeyedUnarchiver unarchiveObjectWithData:[self readObjectforKey:KEY_SCORES]];
    if (scores == NULL || !scores) {
        scores = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:0], nil];
    }
    [scores retain];
    
    NSSortDescriptor* descriptor = [[NSSortDescriptor alloc] initWithKey:@"self" ascending:NO];
    [scores sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    
    if ([scores count] > SCORE_LIMIT) {
        for (int cnt = SCORE_LIMIT; cnt < [scores count]-1; cnt++) {
            [scores removeObjectAtIndex:cnt];
        }
    }
    
    [scores sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    if ([NSKeyedUnarchiver unarchiveObjectWithData:[self readObjectforKey:KEY_MUSIC]] == nil) {
        musicOn = true;
        sfxOn = true;
    } else {
        musicOn = [(NSNumber*)[NSKeyedUnarchiver unarchiveObjectWithData:[self readObjectforKey:KEY_MUSIC]] boolValue];
        sfxOn = [(NSNumber*)[NSKeyedUnarchiver unarchiveObjectWithData:[self readObjectforKey:KEY_SOUND]] boolValue];
    }
    
    if ([NSKeyedUnarchiver unarchiveObjectWithData:[self readObjectforKey:KEY_CURRENCY]] == nil) {
        steeringWheels = 0; /* 0 Steering Wheels at the beginning */
    } else {
        steeringWheels = [(NSNumber*)[NSKeyedUnarchiver unarchiveObjectWithData:[self readObjectforKey:KEY_CURRENCY]] intValue];
    }
    
    if (!musicOn) [[SoundManager getInstance] pauseMusic];
    [self saveData];
    //    [scores sortUsingSelector:@selector(compare:)];
}

- (void)renderFade {
    glDisable(GL_TEXTURE_2D);
    GLshort vertices [] = {
        0.0, (short)h,
        (short)w, (short)h,
        0.0, 0.0,
        (short)w, 0.0,
    };
    
    glColor4f(0, 0, 0, fadeAlpha);
    glVertexPointer(2, GL_SHORT, 0, vertices);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glEnable(GL_TEXTURE_2D);
}

- (void)addToScore:(int)amount {
    /* Set ingame currency */
    steeringWheels += amount;
    [self saveData];
    [self reportScore:steeringWheels forLeaderboardID:@"com.taxipanik.leaderboards.richscore"];
}

- (void)pause {
    if (state == PLAY_GAME) {
        [[MenuManager getInstance] destroy];
        [[MenuManager getInstance] addElement:BUTTON rect:CGRectMake(10, h-60, 100, 50) angle:0 title:BUTTON_MENU];
    }
    paused = true;
}

- (void) resetGame {
    score = 0;
    gameSpeed = SLOW;
    timer = 0.0;
    timeinterval = 20;
    increaseDifficultyTimer = timeinterval;
    customerSpawnTimer = 10.0;
    playGameCountdown = 0;
    //            startGame = false;
    countDownNumber = 3;
    [gameObjects removeAllObjects];
    [destroyable removeAllObjects];
    [objectsToAdd removeAllObjects];
    
    score = 0;
    currentLevel.numPlayers = 4;
    currentLevel = nil;
    
    costToContinue = 1;
    spawnWheelTimer = 1000;
}

- (void) reportScore: (int64_t) highscore forLeaderboardID: (NSString*) category
{
    if (!((AppDelegate*)[[UIApplication sharedApplication] delegate]).gameCenterEnabled) return;
    GKScore *scoreReporter = [[GKScore alloc] initWithCategory:category];
    scoreReporter.value = highscore;
    scoreReporter.context = 0;
    
    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
        // Do something interesting here.
        if (error != nil)
            NSLog(@"%@", error.localizedDescription);
    }];
}

- (void) reportAchievementIdentifier: (NSString*) identifier
{
    if (!((AppDelegate*)[[UIApplication sharedApplication] delegate]).gameCenterEnabled) return;
    GKAchievement *achievement = [[GKAchievement alloc] initWithIdentifier: identifier];
    if (achievement)
    {
        achievement.percentComplete = 100;
        [achievement setShowsCompletionBanner:true];
        [achievement reportAchievementWithCompletionHandler:^(NSError *error)
         {
             if (error != nil)
             {
                 NSLog(@"Error in reporting achievements: %@", error);
             }
         }];
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    [gameCenterViewController dismissModalViewControllerAnimated:YES];
    [((AppDelegate*)[[UIApplication sharedApplication] delegate]) startLoop];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        /* ok */
        [self addToScore:-costToContinue];
        costToContinue*=2;
        currentLevel.numPlayers += 1; /* Give the Player one extra car */
        [((AppDelegate*)[[UIApplication sharedApplication] delegate]) startLoop];
    } else {
        [self newRandomChunkTypeArray];
        [scores addObject:[NSNumber numberWithInt:score]];
        
        NSSortDescriptor* descriptor = [[NSSortDescriptor alloc] initWithKey:@"self" ascending:NO];
        [scores sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        
        if ([scores count] > SCORE_LIMIT) {
            for (int cnt = SCORE_LIMIT; cnt < [scores count]-1; cnt++) {
                [scores removeObjectAtIndex:cnt];
            }
        }
        
        if ([scores count] == SCORE_LIMIT) {
            for (NSNumber* num in scores) {
                if ([num intValue] < score) {
                    [scores removeObject:num];
                    [scores addObject:[NSNumber numberWithInt:score]];
                    break;
                }
            }
        }
        [scores sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        
        [self saveData];
        
        [self reportScore:(int64_t)score forLeaderboardID:@"com.taxipanik.leaderboards.highscore"];
        if (score < 0) [self reportAchievementIdentifier:@"com.taxipanik.achievements.under0"];
        
        state = LOAD_WON_GAME;
        fadeIn = true;
        costToContinue = 1;
        [((AppDelegate*)[[UIApplication sharedApplication] delegate]) startLoop];
    }
}

@end