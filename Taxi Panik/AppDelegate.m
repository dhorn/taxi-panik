//
//  AppDelegate.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

#import "Math.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize payManager;
@synthesize localPlayer = _localPlayer;
@synthesize gameCenterEnabled;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] init] autorelease];
    self.window.screen = [UIScreen mainScreen];
    self.window.windowLevel = UIWindowLevelNormal;
    
    /* Inapp */
    self.payManager = [[PaymentManager alloc] init];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self.payManager];
    
    /* GameCenter */
    [self authenticateLocalPlayer];
    
    mainViewController = [[[ViewController alloc] init] autorelease];
    [(MainView*)mainViewController.view setupOGL];
    mainViewController.view.backgroundColor = [UIColor grayColor];
    self.window.rootViewController = mainViewController;
//    [self.window addSubview:mainViewController.view];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void) authenticateLocalPlayer
{
    /*self.localPlayer = [GKLocalPlayer localPlayer];
    self.localPlayer.authenticateHandler = ^(UIViewController *viewC, NSError *error)
    {
        if (viewC != nil)  {
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentModalViewController:viewC
                                                                                                          animated:YES];
            //            [self showAuthenticationDialogWhenReasonable: viewC];
        } else if (self.localPlayer.isAuthenticated) {
            self.gameCenterEnabled = true;
        } else {
            self.gameCenterEnabled = false;
        }
    };*/
//    [((AppDelegate*)[[UIApplication sharedApplication] delegate]) startLoop];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
//    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [self stopLoop];
    [[GameManager getInstance] saveData];
    [[GameManager getInstance] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    [[GameManager getInstance] loadData];
    [self startLoop];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self stopLoop];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    NSLog(@"I'll count to three! Clear memory!!! 1... 2...");
    [[GameManager getInstance] saveData];
}

- (void)loop {
    //draw
    [mainViewController.view drawRect:[UIScreen mainScreen].applicationFrame];
//    if (loop) {
//        [NSTimer scheduledTimerWithTimeInterval:0.0
//                                         target:self
//                                       selector:@selector(loop)
//                                       userInfo:nil
//                                        repeats:false];
//    }
}

- (void)startLoop {
//    loop = true;
//    [self loop];
    if (!gameTimer) {
        gameTimer = [[CADisplayLink displayLinkWithTarget:self selector:@selector(loop)] retain];
        gameTimer.frameInterval = FRAMERATE;
        [gameTimer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    }
    [[GameManager getInstance] start];
}

- (void)stopLoop {
//    loop = false;
    [[GameManager getInstance] stop];
    [timer invalidate];
    timer = nil;
}

@end
