//
//  StreetChunk.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Chunk.h"

@interface StreetChunk : Chunk {
    BOOL isFork, blocked;
    float timer;
}

@property BOOL isFork;
@property BOOL blocked;

@end
