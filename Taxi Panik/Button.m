//
//  Button.m
//  Taxi Panik
//
//  Created by Dominik Horn on 13.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Button.h"
#import "GameManager.h"
#import "Font.h"

@implementation Button

- (id)init
{
    self = [super init];
    if (self) {
        texCoordMaxY = 0.5;
        texCoordMaxX = 0.5;
        texCoordMinY = 0.0;
        texCoordMinX = 0.0;
        selected = false;
    }
    return self;
}
                                //Point now      //Point before
- (BOOL)receiveEvent:(int)event point:(CGPoint)p point2:(CGPoint)p2 {
    //check Button pos
    if (!(rect.origin.x-TOLTOUCH_X <= p.x && rect.origin.x+TOLTOUCH_X+rect.size.width  >= p.x &&
        rect.origin.y-TOLTOUCH_Y <= p.y && rect.origin.y+TOLTOUCH_Y+rect.size.height >= p.y)) {
        texCoordMaxY = 0.5;
        texCoordMaxX = 0.5;
        texCoordMinY = 0.0;
        texCoordMinX = 0.0;
        selected = false;
        return false;
    }
    //Do stuff based on event
    switch (event) {
        case EVENTTOUCHDOWN:
            texCoordMaxY = 1.0;
            texCoordMaxX = 0.5;
            texCoordMinY = 0.5;
            texCoordMinX = 0.0;
            selected = true;
            return true;
            break;
        case EVENTTOUCHUP:
            texCoordMaxY = 0.5;
            texCoordMaxX = 0.5;
            texCoordMinY = 0.0;
            texCoordMinX = 0.0;
            if (selected) {
                [[GameManager getInstance] buttonEvent:self.title];
                [[SoundManager getInstance] playSound:SOUND_BUTTON_PRESS];
            }
            selected = false;
            return true;
            break;
        case EVENTTOUCHCANCELED: //same as touchup, except not User triggered
            texCoordMaxY = 0.5;
            texCoordMaxX = 0.5;
            texCoordMinY = 0.0;
            texCoordMinX = 0.0;
            //[[GameManager getInstance] buttonEvent:self.title];
            selected = false;
            return true;
        case EVENTTOUCHMOVE: //We want to make other UI Elements receive touch moves!!!
            if ((rect.origin.x-TOLTOUCH_X <= p.x && rect.origin.x+TOLTOUCH_X+rect.size.width  >= p.x &&
                  rect.origin.y-TOLTOUCH_Y <= p.y && rect.origin.y+TOLTOUCH_Y+rect.size.height >= p.y)) {
                texCoordMaxY = 1.0;
                texCoordMaxX = 0.5;
                texCoordMinY = 0.5;
                texCoordMinX = 0.0;
                selected = true;
            }
        default:
            break;
    }
    return false; //return false by default, if event was ignored for example 
}

- (void)renderName {
    glPushMatrix();
    int borderX = self.rect.size.width-self.title.length*WIDTH/3;
    int borderY = self.rect.size.height-HEIGHT;
    if (borderX >= 0 && borderY >= 0) {
        glTranslatef(self.rect.origin.x + borderX/3, self.rect.origin.y + borderY/3, 0.0);
    } else {
        glTranslatef(self.rect.origin.x, self.rect.origin.y, 0.0);
        glScalef(1.0, 1.0, 1.0);
    }
    [[Font getInstance] drawString:self.title
                                at:CGPointMake(self.rect.size.width/10.0,
                                               self.rect.size.height/10.0)];
    glPopMatrix();
}

@end
