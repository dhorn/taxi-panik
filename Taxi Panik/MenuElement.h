//
//  MenuElement.h
//  Taxi Panik
//
//  Created by Dominik Horn on 13.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextureManager.h"
#import "Math.h"

#define TOLTOUCH_X 5
#define TOLTOUCH_Y 10

@class GameManager;

enum EVENTS {
    EVENTTOUCHDOWN,
    EVENTTOUCHMOVE,
    EVENTTOUCHUP,
    EVENTTOUCHCANCELED,
}; 

@interface MenuElement : NSObject {
    BOOL touchDown;
    NSString* textureName;  //TextureName
    CGRect rect;            //rect
    float angle;            //rotation
    NSString* title;
    float texCoordMaxX, texCoordMaxY, texCoordMinX, texCoordMinY;
}

- (BOOL)receiveEvent:(int)event point:(CGPoint)p point2:(CGPoint)p2; // returns wether element is used or not
- (void)render;
- (void)renderName;

@property BOOL touchDown;
@property CGRect rect;
@property float angle;
@property (retain) NSString* textureName;
@property (retain) NSString* title;

@end
