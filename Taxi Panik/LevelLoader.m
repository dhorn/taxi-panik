//
//  LevelLoader.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LevelLoader.h"

@implementation LevelLoader
#pragma mark -
#pragma mark Public
- (Level*)loadLevel:(NSString*)levelName {
    UIImage* level = [UIImage imageNamed:levelName];
    [self loadImageData:level];
    Level* world = [[Level alloc] init];
    int pixelCnt = -1;
    int rowCnt = 0;
    
    
    int numStreets = 0;
    int numGrass   = 0;
    int numHouses  = 0;
    int numSnow    = 0;
    
    for (UIColor* color in data) {
        if (pixelCnt == ([level size].width-1)) {
            rowCnt++;
            pixelCnt = -1;
        }
        pixelCnt++;
        
        const CGFloat* c = CGColorGetComponents([color CGColor]);
        float red = c[0];
        float green = c[1];
        float blue = c[2];
        
        //alpha not needed for the game right now
        //Black = Street
        if (red*255 == 0.0 && green*255 == 0.0 && blue*255 == 0.0) {
            numStreets++;
            NSMutableArray* array = [NSMutableArray arrayWithCapacity:4];
            [array addObject:[OPoint pwp:CGPointMake((pixelCnt-1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt-1)*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake((pixelCnt+1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt+1)*CHUNKSIZE)]];
            StreetChunk* chunk = [[StreetChunk alloc] init];
            chunk.type = STREET_STRAIGHT;
            chunk.rect = CGRectMake(pixelCnt*CHUNKSIZE, rowCnt*CHUNKSIZE, CHUNKSIZE, CHUNKSIZE);
            chunk.angle = 0.1;
            chunk.isFork = false;
            chunk.neighboors = [array copy];
            [world.chunks addObject:chunk];
        }
//        //Grey = Street that can spawn a car at Level Start
//        if (red*255 == 127.5 && green*255 == 127.5 && blue*255 == 127.5) {
//            NSMutableArray* array = [NSMutableArray arrayWithCapacity:4];
//            [array addObject:[OPoint pwp:CGPointMake((pixelCnt-1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
//            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt-1)*CHUNKSIZE)]];
//            [array addObject:[OPoint pwp:CGPointMake((pixelCnt+1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
//            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt+1)*CHUNKSIZE)]];
//            StreetChunk* chunk = [[StreetChunk alloc] init];
//            chunk.type = STREET_STRAIGHT;
//            chunk.pos = CGPointMake(pixelCnt*CHUNKSIZE, rowCnt*CHUNKSIZE);
//            chunk.angle = 0.1;
//            chunk.neighboors = [array copy];
//            chunk.canspawnCar = true;
//            [world.chunks addObject:chunk];
//        }
        //Green = Grass
        if (red*255 == 0.0 && green*255 == 249 && blue*255 == 0.0) {
            numGrass++;
            NSMutableArray* array = [NSMutableArray arrayWithCapacity:4];
            [array addObject:[OPoint pwp:CGPointMake((pixelCnt-1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt-1)*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake((pixelCnt+1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt+1)*CHUNKSIZE)]];
            DecorationChunk* chunk = [[DecorationChunk alloc] init];
            chunk.type = DECORATION_GRASS;
            chunk.rect = CGRectMake(pixelCnt*CHUNKSIZE, rowCnt*CHUNKSIZE, CHUNKSIZE, CHUNKSIZE);
            chunk.angle = 0;
            [world.chunks addObject:chunk];
        }
        //Brown = House
        NSLog(@"color: %f, %f, %f", red*255, green*255, blue*255);
        if (red*255 == 148 && green*255 == 82  && blue*255 == 0.0) {
            numHouses++;
            NSMutableArray* array = [NSMutableArray arrayWithCapacity:4];
            [array addObject:[OPoint pwp:CGPointMake((pixelCnt-1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt-1)*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake((pixelCnt+1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt+1)*CHUNKSIZE)]];
            DecorationChunk* chunk = [[DecorationChunk alloc] init];
            chunk.type = DECORATION_HOUSE;
            chunk.rect = CGRectMake(pixelCnt*CHUNKSIZE, rowCnt*CHUNKSIZE, CHUNKSIZE, CHUNKSIZE);
            chunk.angle = 0;
            [world.chunks addObject:chunk];
        }
        //White = Snow
        if (red*255 == 255 && green*255 == 255 && blue*255 == 255) {
            numSnow++;
            NSMutableArray* array = [NSMutableArray arrayWithCapacity:4];
            [array addObject:[OPoint pwp:CGPointMake((pixelCnt-1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt-1)*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake((pixelCnt+1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt+1)*CHUNKSIZE)]];
            DecorationChunk* chunk = [[DecorationChunk alloc] init];
            chunk.type = DECORATION_SNOW;
            chunk.rect = CGRectMake(pixelCnt*CHUNKSIZE, rowCnt*CHUNKSIZE, CHUNKSIZE, CHUNKSIZE);
            chunk.angle = 0;
            [world.chunks addObject:chunk];
        }
        
//        //Grey = Fork
//        if (red*255 == 128 && green*255 == 128  && blue*255 == 128) {
//            numStreets++;
//            NSMutableArray* array = [NSMutableArray arrayWithCapacity:4];
//            [array addObject:[OPoint pwp:CGPointMake((pixelCnt-1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
//            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt-1)*CHUNKSIZE)]];
//            [array addObject:[OPoint pwp:CGPointMake((pixelCnt+1)*CHUNKSIZE, rowCnt*CHUNKSIZE)]];
//            [array addObject:[OPoint pwp:CGPointMake(pixelCnt*CHUNKSIZE, (rowCnt+1)*CHUNKSIZE)]];
//            StreetChunk* chunk = [[StreetChunk alloc] init];
//            chunk.type = STREET_STRAIGHT;
//            chunk.rect = CGRectMake(pixelCnt*CHUNKSIZE, rowCnt*CHUNKSIZE, CHUNKSIZE, CHUNKSIZE);
//            chunk.angle = 0.1;
//            chunk.isFork = true;
//            chunk.neighboors = [array copy];
//            [world.chunks addObject:chunk];
//        }
    }
    
    
    
    NSArray* enumHelper = [NSArray arrayWithArray:world.chunks];
    for (Chunk* chunk in world.chunks) {
        NSMutableArray* pixels = [[NSMutableArray alloc] initWithCapacity:8];
        for (Chunk* chunk2 in enumHelper) {
            for (OPoint* p in chunk.neighboors) {
                if (chunk2.rect.origin.x == p.pos.x && chunk2.rect.origin.y == p.pos.y) {
                    [pixels addObject:chunk2];
                }
            }
        }
        chunk.neighboors = [pixels copy];
        [pixels removeAllObjects];
    }
    for (Chunk* chunk in world.chunks) {
        if ([chunk isKindOfClass:[StreetChunk class]]) {
            BOOL cleftAndRight = false;
            for (Chunk* chunk2 in chunk.neighboors) {
                CGPoint vector2D = CGPointMake(chunk2.rect.origin.x-chunk.rect.origin.x, chunk2.rect.origin.y-chunk.rect.origin.y);
                if ([chunk2 isKindOfClass:[StreetChunk class]]) {
                    if (vector2D.x == 0 && vector2D.y < 0) {
                    //Top
                    chunk.angle = 0;
                }
                if (vector2D.x < 0 && vector2D.y == 0) {
                    //Left
                    if (chunk.angle == 0) {
                        chunk.type  = STREET_TURN;
                        chunk.angle = 180;
                    } else {
                        chunk.angle = 270;
                    }
                }
                if (vector2D.x > 0 && vector2D.y == 0) {
                    //Right
                    if (chunk.angle == 0) {
                        chunk.angle = 270;
                        chunk.type  = STREET_TURN;
                    } else if (chunk.angle == 180) {
                        chunk.type  = STREET_3WAYCROSS;
                        chunk.angle = 270;
                    } else if (chunk.angle == 270) {
                        cleftAndRight = true;
                    } else {
                        chunk.angle = 90;
                    }
                }
                if (vector2D.x == 0 && vector2D.y > 0) {
                    //Bottom
                    if (chunk.angle == 0.1) {
                        chunk.angle = 180;
                    }
                    if (chunk.angle == 0) {
                        //Does Work
                    }
                    if (chunk.angle == 90) {
                        chunk.type  = STREET_TURN;
                        chunk.angle = 0;
                    }
                    if (chunk.angle == 180) {
                        chunk.type  = STREET_3WAYCROSS;
                        chunk.angle = 180;
                    }
                    if (chunk.angle == 270) {
                        switch (chunk.type) {
                            case STREET_STRAIGHT:
                                if (cleftAndRight) {
                                    chunk.type  = STREET_3WAYCROSS;
                                    chunk.angle = 90;
                                } else {
                                    chunk.type  = STREET_TURN;
                                    chunk.angle = 90;
                                }
                                break;
                            case STREET_TURN:
                                chunk.type  = STREET_3WAYCROSS;
                                chunk.angle = 0;
                                break;
                            case STREET_3WAYCROSS:
                                chunk.type  = STREET_4WAYCROSS;
                                chunk.angle = 0;
                                break;
                            default:
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    [world.chunkCounts setObject:[NSNumber numberWithInt:numStreets] forKey:NUMSTREETCHUNKS];
    [world.chunkCounts setObject:[NSNumber numberWithInt:numGrass]   forKey:NUMGRASSCHUNKS];
    [world.chunkCounts setObject:[NSNumber numberWithInt:numHouses]  forKey:NUMHOUSECHUNKS];
    
    world.numPlayers = 4;   /* Maximum of 4 Cars */
    
    return world;
}

#pragma mark -
#pragma mark Intern
- (void)loadImageData:(UIImage*)image {
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    data = [NSMutableArray arrayWithCapacity:width*height];
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    for (int count = 0 ; count < width*height*4 ; count+=4)
    {
        CGFloat red   = (rawData[count]     * 1.0) / 255.0;
        CGFloat green = (rawData[count + 1] * 1.0) / 255.0;
        CGFloat blue  = (rawData[count + 2] * 1.0) / 255.0;
        CGFloat alpha = (rawData[count + 3] * 1.0) / 255.0;
        
        UIColor *acolor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
        [data addObject:acolor];
    }
    
    free(rawData);
}

@end
