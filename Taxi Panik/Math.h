//
//  Math.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Sprite;
@class GameObject;

#define cnstSqrt sqrtf(width/2*width/2+height/2*height/2)

enum CollisionTypes {
    COLBS,
    COLNOROTATE,
    COLROTATE,
    COLPERPIXEL
};

struct Vector {
    float x;
    float y;
    float z;
};
typedef struct Vector Vector;

struct Line {
    float m;
    float t;
};
typedef struct Line Line;

struct CollisionBox {
    CGPoint min; //downLeft
    CGPoint max; //topRight
        
    CGPoint topLeft;
    CGPoint topRight;
    CGPoint downRight;
    CGPoint downLeft;
    
    float angle;
};
typedef struct CollisionBox CollisionBox;

CollisionBox makeCollisionBox(CGRect rect);
CollisionBox calcOOCollisionBox(CollisionBox box, float angle);
Line makeLine(float m, float t);
Vector makeVector(float x, float y, float z);

Vector normalize(Vector v);
float getRad(float grad);
float getGrad(float rad);
float squareDistCCOfRects(CGRect rect1, CGRect rect2);

BOOL sphereCol(GameObject* obj1, GameObject* obj2);
BOOL aabbCol(GameObject* obj1, GameObject* obj2);
BOOL oobbCol(GameObject* obj1, GameObject* obj2);
BOOL pointRectCol(CGRect r, CGPoint p);
BOOL rectRectCol(CGRect r1, CGRect r2);

float dotProduct(Vector vec1, Vector vec2);
//float angleBetween(Vector vec1, Vector vec2);
float angle2D(Vector direction, Vector face);

@interface Math : NSObject {

}
//class Methods
+ (Math*)instance;

//Collision

@end
