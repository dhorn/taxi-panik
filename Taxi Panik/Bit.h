//
//  Bit.h
//  Taxi Panik
//
//  Created by Dominik Horn on 09.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Bit : NSObject {
    BOOL solid;
    CGPoint position;
    
    UIColor* color;
}

@property BOOL solid;
@property CGPoint position;
@property (nonatomic, retain) UIColor* color;

@end
