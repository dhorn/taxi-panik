//
//  LevelLoader.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Level.h"
#import "OPoint.h"
#import "StreetChunk.h"
#import "DecorationChunk.h"
#import "InteractionChunk.h"

@interface LevelLoader : NSObject {
    NSMutableArray* data;
}

- (Level*)loadLevel:(NSString*)levelName;

@end
