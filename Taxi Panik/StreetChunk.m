//
//  StreetChunk.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StreetChunk.h"
#import "GameManager.h"

@implementation StreetChunk
@synthesize isFork, blocked;

- (id)init
{
    self = [super init];
    if (self) {
        self.isFork = false;
        blocked = false;
    }
    return self;
}

- (void)update {
    if (isFork) {
        timer += deltatime;
        NSLog(@"Timer: %f", timer);
        if (timer > 5.0) {
            timer = 0;
            blocked = false;
            int random = arc4random() % 10;
            if (random == 0) {
                blocked = true;
                timer = (float)(arc4random() % 5) - 10; // be blocked for at most 10 seconds
            }
        }
    }
}

- (void)render {
    [super render];
    if (isFork && blocked) {
        glPushMatrix();
        glColor4f(1, 1, 1, 1);
        glDisable(GL_TEXTURE_2D);
        
        glTranslatef(self.rect.origin.x, self.rect.origin.y, 0);
        
        GLshort vertices[] = {
            0.0, 0.0,
            0.0, 32.0,
            32.0, 0.0,
            32.0, 32.0
        };
        
        glVertexPointer(2, GL_SHORT, 0, vertices);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        
        glEnable(GL_TEXTURE_2D);
        glPopMatrix();
    }
}

@end
