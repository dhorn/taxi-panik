//
//  Player.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Player.h"
#import "GameManager.h"

@implementation Player
@synthesize oldPathPoints = _oldpathPoints;
@synthesize pathPoints = _pathPoints;
@synthesize initAppearTimer = initAppearTimer;
@synthesize displayOldPathPoints = displayOldPathPoints;

- (id)init
{
    self = [super init];
    if (self) {
        _pathPoints = [[NSMutableArray alloc] init];
        _oldpathPoints = [[NSMutableArray alloc] init];
        rotateTo = 0.0;
        rotate = false;
        tickcount = 0;
        texTick = 0;
        initAppearTimer = 3;
    }
    return self;
}

- (id)initWithTexture:(NSString *)texture rect:(CGRect)r speed:(Vector)s ai:(int)ai angle:(float)a {
    if (self = [super initWithTexture:texture rect:r speed:s ai:ai angle:a]) {
        _pathPoints = [[NSMutableArray alloc] init];
        _oldpathPoints = [[NSMutableArray alloc] init];
        rotateTo = 0.0;
        rotate = false;
        tickcount = 0;
        initAppearTimer = 3;
    }
    return self;
}

- (void)update {
    if (initAppearTimer > 0) {
        initAppearTimer -= deltatime;
        if (initAppearTimer <= 0) initAppearTimer = 0;
        
        speed.x = 0;
        speed.y = 0;
        texCoords = CGRectMake(0.5, 0, 1, 1);
        return;
    }
    [super update];
    
    //Texture Stuff
    texCounter += deltatime;
    if (texCounter > 1.0/4.0) {
        texCounter -= 1.0/4.0;
        [self switchAnimationFrame];
    }
    
    if (self.angle >= 360.0) {
        angle -= 360.0;
    }
    if (self.angle < 0.0) {
        angle += 360.0;
    }
    if (!displayOldPathPoints && !rotate) {
        int i = -1; //Current Index
        NSMutableArray* tmpArray = [NSMutableArray arrayWithArray:self.oldPathPoints];

        
        for (Chunk* chunk in self.oldPathPoints) {  // Check for chunks on our way.
            if ([self.oldPathPoints indexOfObject:chunk] == 0 && self.oldPathPoints.count < 2
                && pointRectCol(chunk.rect, CGPointMake(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/2))) {
                [self.oldPathPoints removeAllObjects];
                break;
            }
            i++;
            if (![chunk isKindOfClass:[Chunk class]]) { continue; }
            if (self.oldPathPoints.count < i+2) { break; }
            GameObject* g = [[GameObject alloc] init];
            g.rect = CGRectMake(rect.origin.x+rect.size.width/2 - 1, rect.origin.y+rect.size.height/2 - 1, 2, 2);
            if (aabbCol(g, chunk)/*pointRectCol(chunk.rect, CGPointMake(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/2))*/
                && self.oldPathPoints.count >= i) { // We are on that chunk
                nextChunk = [self.oldPathPoints objectAtIndex:i+1];  // The chunk we have to drive to
                Vector move = makeVector(nextChunk.rect.origin.x - chunk.rect.origin.x, nextChunk.rect.origin.y - chunk.rect.origin.y, 0.0);
                //move = normalize(move);
                if (move.x > 0.0 && move.y == 0.0) {
                    rotateTo = 90;
                    if (angle == 0.0) { deltaAngle = 90; }
                    else if (angle == 180) { deltaAngle = -90; }
                    else if (angle == 270) { angle = rotateTo; rotate = false; break; }    // Rotate instantly
                } else if (move.x < 0.0 && move.y == 0.0) {
                    rotateTo = 270;
                    if (angle == 180) { deltaAngle = 90; }
                    else if (angle == 0) { deltaAngle = -90; }
                    else if (angle == 90) { angle = rotateTo; rotate = false; break; }     // Rotate instantly
                } else if (move.x == 0.0 && move.y > 0.0) {
                    rotateTo = 180;
                    if (angle == 90) { deltaAngle = 90; }
                    else if (angle == 270) { deltaAngle = -90; }
                    else if (angle == 0.0) { angle = rotateTo; rotate = false; break; }    // Rotate instantly
                } else if (move.x == 0.0 && move.y < 0.0) {
                    rotateTo = 0.0;
                    if (angle == 270) { deltaAngle = 90; }
                    else if (angle == 90) { deltaAngle = -90; }
                    else if (angle == 180) { angle = rotateTo; rotate = false; break; }    // Rotate instantly 
                }
                if (angle-rotateTo != 0.0) {
                    rotate = true;
                    currentChunkToRotateOn = [chunk retain];
                }
                [self.oldPathPoints removeObject:chunk];
                break;
            }
            if (!aabbCol(g, chunk)){
                [tmpArray removeObject:chunk];
            }
        }
        if (tmpArray.count == 1) [tmpArray removeAllObjects];
        self.oldPathPoints = [NSMutableArray arrayWithArray:tmpArray];
    }
    else if (!displayOldPathPoints && rotate) {
        float dist, move, percent;
        if (rotateTo == 90.0 || rotateTo == 270.0) {
            dist = (rect.origin.y+rect.size.height/2)-(nextChunk.rect.origin.y+nextChunk.rect.size.height/2);
            dist = sqrtf(dist*dist);    /* Only Positive Numbers */
            move = speed.y*deltatime; move = sqrtf(move*move);
            percent = move/dist;
            angle += deltaAngle*percent;
        }
        if (rotateTo == 0.0 || rotateTo == 180.0) {
            dist = (rect.origin.x+rect.size.width/2)-(nextChunk.rect.origin.x+nextChunk.rect.size.width/2);
            dist = sqrtf(dist*dist);    /* Only Positive Numbers */
            move = speed.x*deltatime; move = sqrtf(move*move);
            percent = move/dist;
            angle += deltaAngle*percent;
        }
        /* if (squareDistCCOfRects(currentChunkToRotateOn.rect, self.rect) > 30*30) {} */
        float cutOff = (SPEED_SLOW*2)/100.0;
        switch (gameSpeed) {
            case MEDIUM:
                cutOff = (SPEED_MEDIUM*2)/80.0;
                break;
            case FAST:
                cutOff = (SPEED_FAST*2)/70.0;
                break;
            case INSANE:
                cutOff = (SPEED_INSANE*2)/60.0;
                break;
            default:
                break;
        }
        if (sqrt(deltaAngle*percent*deltaAngle*percent) <= cutOff) {   // Change 0.3 parameter depending on speed; fps
//            NSLog(@"%f", sqrt((rotateTo - angle)*(rotateTo - angle)));
//            if (sqrt((rotateTo - angle)*(rotateTo - angle)) > 8) {    //We jumped more than 5°
//                [[SoundManager getInstance] playSound:SOUND_SQUEAL];
//            }
            angle = rotateTo;
            rotate = false;
            [currentChunkToRotateOn release];
            currentChunkToRotateOn = nil;
        }
        if (deltaAngle > 0.0 || deltaAngle < 0.0) {
            deltaAngle -= deltaAngle*percent;
        }
        if (rotateTo == 0.0 && deltaAngle > 0.0 && angle > 350.0) {
            angle = 0.0;
            rotate = false;
            [currentChunkToRotateOn release];
            currentChunkToRotateOn = nil;
        }
        if ((angle < rotateTo && angle > rotateTo - 2.0) || (angle > rotateTo && angle < rotateTo + 2.0)) {
            angle = rotateTo;
            rotate = false;
            [currentChunkToRotateOn release];
            currentChunkToRotateOn = nil;
        }
        if (sqrt(deltaAngle*percent*deltaAngle*percent) > 10) {  // We Jumped more than 10 degrees
            [[SoundManager getInstance] playSound:SOUND_SQUEAL];
        }
    }
}

- (void)move {
    if (initAppearTimer > 0) return;
    if (displayOldPathPoints) { return; }
    speed.x =  sin(getRad(self.angle));
    speed.y = -cos(getRad(self.angle));
    float gspeed = 0;
    
    switch (gameSpeed) {
        case SLOW:
            gspeed = SPEED_SLOW;
            break;
        case MEDIUM:
            gspeed = SPEED_MEDIUM;
            break;
        case FAST:
            gspeed = SPEED_FAST;
            break;
        case INSANE:
            gspeed = SPEED_INSANE;
            break;
        default:
            NSLog(@"No Game Speed set!!!");
            break;
    }
    
    speed.x *= gspeed;
    speed.y *= gspeed;
    [super move];
}

- (void)toggleDisplayOldPathPoints {
    if (!displayOldPathPoints) { if (self.oldPathPoints.count < 2) { [self.oldPathPoints removeAllObjects]; } }
    displayOldPathPoints = displayOldPathPoints ? false : true;     //Toggle display Boolean
}

- (void)render {
    //Render current selection
    if (displayOldPathPoints || self.oldPathPoints.count < 5) {
        for (Chunk* chunk in self.oldPathPoints) {
            if (![chunk isKindOfClass:[Chunk class]]) { continue; }
            [[GameManager getInstance] drawOGLRect:chunk.rect color:makeVector(0.5, 0.0, 0.0) alpha:0.3];
        }
    }
    
    for (Chunk* chunk in self.pathPoints) {
        if (![chunk isKindOfClass:[Chunk class]]) { continue; }
        [[GameManager getInstance] drawOGLRect:chunk.rect color:makeVector(0.0, 0.5, 0.0) alpha:0.3];
    }
    
    
    
    //Car has to be on top of all other layers etc
    if (self.oldPathPoints.count == 0) { /* Blinking effect when the car ran out of chunks in the oldpathpoints array */
        if (!displayOldPathPoints) {
            blinkCounter += deltatime;
            if (blinkCounter > 0.3 && blinkCounter < 0.7) {
                [super render];
            } else if (blinkCounter >= 0.7) {
                blinkCounter-=0.7;
            }
        } else {
            [super render];
        }
    } else {
        [super render];
    }
    
    //[[GameManager getInstance] drawOGLRect:self.rect color:makeVector(1.0, 0.0, 0.0) alpha:1.0];

    
    // Don't Render timer, since uggly + buggy
    
    
//    if (initAppearTimer > 0) {
//        //tmp, add Animation
//        [[Font getInstance] drawString:[NSString stringWithFormat:@"%.0f", initAppearTimer]
//                                    at:CGPointMake(self.rect.origin.x + self.rect.size.width/2 - 6, self.rect.origin.y + self.rect.size.height/2 - 6)];
//    }
}

- (BOOL)rotate {
    return rotate;
}

- (void)switchAnimationFrame {
    if (texCoords.origin.x == 0) texCoords = CGRectMake(0.5, 0, 1, 1);
    else texCoords = CGRectMake(0, 0, 0.5, 1);
}

@end
