//
//  SoundManager.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SoundManager.h"
#import "GameManager.h"

@implementation SoundManager
@synthesize sounds, isMultithreaded;

#pragma mark -
#pragma mark class Methods
+ (SoundManager*)getInstance {
    static SoundManager* soundManager;
    @synchronized (self) {
        if(!soundManager) {
            soundManager = [[SoundManager alloc] init];
        }
    }
    return soundManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.isMultithreaded = false;
        self.sounds = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)shutDownAudioSystem {
    if (isMultithreaded && [soundThread isExecuting]) {
        [soundThread cancel];
        int cnt = 4;
        while ([soundThread isExecuting]) {
            cnt--;
            if (cnt == 0) {
                NSLog(@"GIVING UP, COULDN'T SHUTDOWN AUDIO SYSTEM");
                return;
            }
            NSLog(@"ERROR: NOT ABLE TO SHUTDOWN SOUNDSYSTEM\nRETRYING %i MORE TIMES", cnt);
            [soundThread cancel];
            sleep(1);
        }
    }
}

#pragma mark -
#pragma mark Methodes
- (BOOL)isPlaingsoundOnSecondThread {
    return self.isMultithreaded;
}

- (void)playSoundOnSecondThread:(BOOL)v {
    NSLog(@"ERROR: NEED TO SETUP MULTITHREADING!");
    self.isMultithreaded = false;
}

- (void)loopSound:(NSString*)name {
    @synchronized (self) {
       CustomAVAudioPlayer *sound = [self getSound:name];
        if (sound) {
            if (!sound.playing) {
                sound.currentTime = 0;
                sound.numberOfLoops = -1;
                if (self.isMultithreaded) {
                    [soundThread performSelector:@selector(play:) onThread:soundThread withObject:sound waitUntilDone:NO];
//                    soundThread = [[NSThread alloc] initWithTarget:self
//                                                          selector:@selector(play:)
//                                                            object:sound];
//                    [soundThread start];
                } else {
                    [sound play];    
                }
            }
        }
    }
}

- (void)playSound:(NSString*)name {
    if ([[GameManager getInstance] isSfxOn]) {
        @synchronized (self) {
           CustomAVAudioPlayer *sound = [self getSound:name];
            if (sound) {
                if (!sound.playing) {
                    sound.currentTime = 0;
                    sound.numberOfLoops = 0;
                    [sound prepareToPlay];
                    if (self.isMultithreaded) {
                        [soundThread performSelector:@selector(play:) onThread:soundThread withObject:sound waitUntilDone:NO];
//                        soundThread = [[NSThread alloc] initWithTarget:self
//                                                          selector:@selector(play:)
//                                                            object:sound];
//                        [soundThread start];
                    } else {
                        [sound play];
                    }
                }
            }
        }
    }
}

- (void)playMusic:(NSString*)name {
    if ([[GameManager getInstance] isMusicOn]) {
        @synchronized (self) {
           CustomAVAudioPlayer *sound = [self getSound:name];
            if (sound) {
                if (!sound.playing) {
                    sound.currentTime = 0;
                    sound.numberOfLoops = 0;
                    sound.delegate = self;
                    sound.volume = 1.0; /* Music is louder */
                    [sound prepareToPlay];
                    if (self.isMultithreaded) {
                        [soundThread performSelector:@selector(play:) onThread:soundThread withObject:sound waitUntilDone:NO];
                    } else {
                        [sound play];
                    }
                }
            }
        }
    }
}

- (void)stopSound:(NSString*)name {
    @synchronized (self) {
       CustomAVAudioPlayer *sound = [self getSound:name];
        if (sound && sound.isPlaying) {
            [sound stop];
        }
    }
}

#pragma mark -
#pragma mark Intern
- (CustomAVAudioPlayer*)getSound:(NSString*)name {
    @try {
        CustomAVAudioPlayer* sound = [self.sounds objectForKey:name];
        if (!sound) {
            NSError *error;
            NSString *path = [[NSBundle mainBundle] pathForResource:name ofType: nil];             
			sound = [[CustomAVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:path]
                                                           error: &error];
            if (!sound) {
                NSLog(@"ERROR: Wrong sound format: %@. Description: %@", name, 
                      [error localizedDescription]);
            } else {
                @synchronized (self) {
                    sound.volume = 0.7;
                    int len = sound.duration;
                    [self.sounds setObject: sound forKey: name];
                    NSLog(@"%@ loaded, duration: %i sec", name, len);
                }
            }
		} 
        return sound;
    }
    @catch (NSException *exception) {
        NSLog(@"ERROR: %@ NOT FOUND", name);
    }
}

#pragma mark -
#pragma mark private
- (void)play:(AVAudioPlayer*)s {
    if (s) {
        [s play];
    }
}

#pragma mark -
#pragma markCustomAVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
//    NSLog(@"AudioPlayer %@ finished %i. Sound loops: %i", player, flag, player.numberOfLoops);
    //we are the delegate => select random new theme song to play!!!
    @synchronized(self) {
        [self performSelectorOnMainThread:@selector(chooseNewThemeSong:)
                               withObject:[(CustomAVAudioPlayer*)player name]
                            waitUntilDone:YES];
    }
}

- (void)playDelayedMusic:(NSString*)name {
    sleep(3); //3 Seconds
    [self playMusic:name];
}

- (void)chooseNewThemeSong:(NSString*)oldName {
    NSString* newName = oldName;
    while ([oldName isEqualToString:newName]) {
        [self generateRandomThemeSongName:&newName];
    }
    [self performSelectorInBackground:@selector(playDelayedMusic:) withObject:newName];
}

- (void)generateRandomThemeSongName:(NSString**)name {
    int rand = (arc4random() % 3) + 1;
    switch (rand) {
        case 1:
            *name = SOUND_THEME_1;
            return;
        case 2:
            *name = SOUND_THEME_2;
            return;
        case 3:
            *name = SOUND_THEME_3;
            return;
        default:
            break;
    }
    *name = @"FAIL_SONG";
}

- (void)mute {
    [self pauseMusic];
//    [self performSelector:@selector(blockUntilNotified) onThread:soundThread withObject:nil waitUntilDone:NO];
}

- (void)unMute {
    [self unPauseMusic];
//    isOnHold = false;
}

- (void)pauseMusic {
    for (CustomAVAudioPlayer* p in [self.sounds allValues]) {
        if (![p isKindOfClass:[CustomAVAudioPlayer class]]) continue;
        
        if (p.isPlaying) {
            p.wasPlaying = true;
            [p pause];
        }
    }
}

- (void)unPauseMusic {
    for (CustomAVAudioPlayer* p in [self.sounds allValues]) {
        if (![p isKindOfClass:[CustomAVAudioPlayer class]]) continue;
        
        if (p.wasPlaying) {
            p.wasPlaying = false;
            [p play];
        }
    }
}

- (void)blockUntilNotified {
    isOnHold = true;
    while (isOnHold) {
        sleep(5);   //Don't melt away the Battery
    }
}

@end
