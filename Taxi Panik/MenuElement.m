//
//  MenuElement.m
//  Taxi Panik
//
//  Created by Dominik Horn on 13.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuElement.h"
#import "GameManager.h"

@implementation MenuElement
@synthesize touchDown, textureName, rect, angle, title;

- (id)init
{
    self = [super init];
    if (self) {
        texCoordMaxX = 1;
        texCoordMaxY = 1;
        texCoordMinX = 0;
        texCoordMinY = 0;
    }
    return self;
}

- (BOOL)receiveEvent:(int)event point:(CGPoint)p point2:(CGPoint)p2 {
    //To be overriden by subclass
    return false;   //Standard element can't recieve Events
}

- (void)render { //Standard rendering, can be overriden
    GLshort vertices [] = {
        0.0, rect.size.height,
        rect.size.width, rect.size.height,
        0.0, 0.0,
        rect.size.width, 0.0,
    };
    GLfloat textureCoords[ ] = {
        texCoordMinX, texCoordMaxY, //links unten
        texCoordMaxX, texCoordMaxY, //rechts unten
        texCoordMinX, texCoordMinY, //links oben
        texCoordMaxX, texCoordMinY  //rechts oben
    };
        
    //glEnable(GL_TEXTURE_2D); //alle Flaechen werden nun texturiert   
    
//    glColor4f(1, 1, 1, 1);
    glBindTexture(GL_TEXTURE_2D, [[TextureManager getInstance] getTextureID:textureName]);
    glVertexPointer(2, GL_SHORT, 0, vertices);
    glTexCoordPointer(2, GL_FLOAT, 0, textureCoords);
    
    glPushMatrix();    
    glTranslatef(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/2, 0.0);
    glRotatef(angle, 0, 0, 1); //angle = 0 = keine Rotation
    glTranslatef(-rect.size.width/2, -rect.size.height/2, 0.0);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);         
    glPopMatrix();
    
    //glDisable(GL_TEXTURE_2D);
    
    [self renderName];
}

- (void)renderName {
    glPushMatrix();
    glScalef(1.1, 1.1, 1.0);
    [[Font getInstance] drawString:self.title
                                at:CGPointMake(self.rect.origin.x + self.rect.size.width/10.0,
                                               self.rect.origin.y + self.rect.size.height/10.0)];
    glPopMatrix();
}

@end
