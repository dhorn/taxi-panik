//
//  Wheel.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.13.
//
//

#import "Wheel.h"
#import "Math.h"
#import "GameManager.h"

@implementation Wheel
@synthesize clicked, direction;

- (id)initWithTexture:(NSString *)texture rect:(CGRect)r speed:(Vector)s ai:(int)ai angle:(float)a {
    self = [super initWithTexture:texture rect:r speed:s ai:ai angle:a];
    if (self) {
        deathTimer = (arc4random() % 90) + 30*3; /* 3 seconds minimum livetime */
        self.clicked = false;
    }
    return self;
}

- (void)update {
    deathTimer--;
    if (!self.clicked && deathTimer <= 0) {
        self.isActive = false;
    }
    if (self.clicked) {
        float distance = sqrtf(squareDistCCOfRects(CGRectMake(rect.origin.x, rect.origin.y, 1, 1), CGRectMake(w-37, 2, 1, 1)));
        distance /= 1.2;
        if (distance < 40.0) distance = 40.0;
        self.speed = makeVector(direction.x * distance, direction.y * distance, 0);
    }
    if (self.isActive && self.clicked && aabbCol(self, [[Sprite alloc] initWithTexture:@""
                                                 rect:CGRectMake(w-37, 2, 32, 32)
                                                speed:makeVector(0, 0, 0)
                                                   ai:0
                                                angle:0])) {
        self.isActive = false;
        [[GameManager getInstance] addToScore:1];   /* add the wheel */
    }
    [super update];
}

- (void)render {
    float alpha = 1.0;
    /* 3 seconds left to die => fading texture*/
    if (deathTimer <= 90 && !self.clicked) {
        alpha = (float)deathTimer / 90.0;
    }
    
    GLshort vertices [] = {
        0.0, rect.size.height,
        rect.size.width, rect.size.height,
        0.0, 0.0,
        rect.size.width, 0.0,
    };
    GLfloat textureCoords[ ] = {
        texCoords.origin.x,    texCoords.size.height, //links unten
        texCoords.size.width,  texCoords.size.height, //rechts unten
        texCoords.origin.x,    texCoords.origin.y,    //links oben
        texCoords.size.width,  texCoords.origin.y     //rechts oben
    };
    
    glColor4f(alpha, alpha, alpha, alpha);
    glBindTexture(GL_TEXTURE_2D, [[TextureManager getInstance] getTextureID:textureName]);
    glVertexPointer(2, GL_SHORT, 0, vertices);
    glTexCoordPointer(2, GL_FLOAT, 0, textureCoords);
    
    glPushMatrix();
    glTranslatef(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/2, 0.0);
    glRotatef(angle, 0, 0, 1); //angle = 0 = keine Rotation
    glTranslatef(-rect.size.width/2, -rect.size.height/2, 0.0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();
    
    glColor4f(1, 1, 1, 1);
}

@end
