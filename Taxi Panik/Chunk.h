//
//  Chunk.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameObject.h"

@class Player;

#define CHUNKSIZE 32.0

enum types {
    STREET_STRAIGHT,
    STREET_TURN,
    STREET_3WAYCROSS,
    STREET_4WAYCROSS,
    DECORATION_GRASS,
    DECORATION_HOUSE,
    DECORATION_SNOW,
    INTERACTION,
};

@interface Chunk : GameObject {
    int type;
//    CGPoint pos;
    BOOL canspawnCar;
    
    NSArray* neighboors;
    NSTimer* playerDeadTimer;
    Player* playerToDie;
}

- (void)render;
- (void)setType:(int)t;
- (int)type;

//@property CGPoint pos;
@property BOOL canspawnCar;
@property (nonatomic, retain) NSArray* neighboors;

@end
