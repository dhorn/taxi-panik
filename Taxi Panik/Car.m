//
//  Car.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Car.h"
#import "GameManager.h"

@implementation Car

- (id)init
{
    self = [super init];
    if (self) {
        self.textureName = @"car_animated.png";
        self.rect = CGRectMake(0.0, 0.0, 28.0, 28.0);
        self.speed = makeVector(0.0, 0.0, 0.0);
        self.aiType = AIDEFAULT;
        texCoords = CGRectMake(0.0, 0.0, 1.0, 1.0);
        self.angle = 0.0;
        self.isActive = true;
    }
    NSLog(@"ERROR, DO NOT USE INIT ON SPRITE OBJECT");
    return self;
}

- (void)update {
    [super update];
}

@end
