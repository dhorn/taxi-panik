//
//  Texture.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Texture : NSObject {
    int textureID;
    int width;
    int height;
}

@property int textureID;
@property int width;
@property int height;

@end
