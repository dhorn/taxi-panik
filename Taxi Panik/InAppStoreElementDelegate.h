//
//  InAppStoreElementDelegate.h
//  Taxi Panik
//
//  Created by Dominik Horn on 22.03.13.
//
//

#import <Foundation/Foundation.h>
@class InAppShopElement;

@protocol InAppStoreElementDelegate <NSObject>

@required
- (void)actionTriggered:(InAppShopElement*)element;

@end
