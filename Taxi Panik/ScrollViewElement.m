//
//  ScrollViewElement.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.11.12.
//
//

#import "ScrollViewElement.h"
#import "TextureManager.h"
#import "Font.h"

@implementation ScrollViewElement
@synthesize name = _name;
@synthesize rect = _rect;
@synthesize selected = _selected;

+ (ScrollViewElement*)scrollViewElementWithRect:(CGRect)rect name:(NSString*)string {
    return [[[ScrollViewElement alloc] initScrollViewElementWithRect:rect name:string] autorelease];
}

- (id)initScrollViewElementWithRect:(CGRect)rect name:(NSString*)string {
    self = [super init];
    if (self) {
        self.selected = false;
        self.rect = rect;
        self.name = string;
    }
    return self;
}

- (void)renderWithYOffset:(float)yOff {
    GLshort vertices [] = {
        0.0, _rect.size.height,
        _rect.size.width, _rect.size.height,
        0.0, 0.0,
        _rect.size.width, 0.0,
    };
//    GLfloat textureCoords[ ] = {
//        0, 1, //links unten
//        1, 1, //rechts unten
//        0, 0, //links oben
//        1, 0  //rechts oben
//    };
//    
//    glBindTexture(GL_TEXTURE_2D, [[TextureManager getInstance] getTextureID:TEXTURE_NAME]);
    glDisable(GL_TEXTURE_2D);
    glColor4f(0, 0, 0, 0.5);
    if (_selected) {
        glColor4f(0, 0, 1, 0.5);
    }
    glVertexPointer(2, GL_SHORT, 0, vertices);
//    glTexCoordPointer(2, GL_FLOAT, 0, textureCoords);
    
    glPushMatrix();
    glTranslatef(_rect.origin.x, _rect.origin.y+yOff, 0.0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();

    glEnable(GL_TEXTURE_2D);
    glColor4f(1, 1, 1, 1);
    float offsetX = WIDTH/3*self.name.length/2.0;
    float offsetY = HEIGHT/2.0;
    [[Font getInstance] drawString:self.name at:CGPointMake((self.rect.origin.x+self.rect.size.width/2)-offsetX,
                                                            (self.rect.origin.y+self.rect.size.height/2)-offsetY+yOff)];

//    glColor4f(1, 1, 1, 1);
//    [[Font getInstance] drawString:self.name at:CGPointMake(self.rect.origin.x, self.rect.origin.y+yOff)];

//    if (_selected) {
//        GLshort vertices [] = {
//            0.0, _rect.size.height,
//            _rect.size.width, _rect.size.height,
//            0.0, 0.0,
//            _rect.size.width, 0.0,
//        };
//        
//        glDisable(GL_TEXTURE_2D);
//        glColor4f(0.0, 0.0, 1.0, 0.3);
//        
//        glVertexPointer(2, GL_SHORT, 0, vertices);
//        
//        glPushMatrix();
//        glTranslatef(_rect.origin.x, _rect.origin.y, 0.0);
//        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
//        glPopMatrix();
//        
//        glEnable(GL_TEXTURE_2D);
//    }
}

@end
