//
//  Sprite.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Sprite.h"

@implementation Sprite
@synthesize aiType, isActive;

- (id)initWithTexture:(NSString*)texture
                 rect:(CGRect)r
                speed:(Vector)s
                   ai:(int)ai
                angle:(float)a {
 
    if (self = [super init]) {
        self.textureName = texture;
        self.rect = r;
//        NSLog(@"\n%f\n%f", rect.origin.x, rect.origin.y);
        self.speed = s;
        self.aiType = ai;
        self.angle = a;
        self.isActive = true;
        texCoords = CGRectMake(0, 0, 1, 1);
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.textureName = @"NoTexture.png";
        self.rect = CGRectMake(0.0, 0.0, 32.0, 32.0);
        self.speed = makeVector(0.0, 0.0, 0.0);
        self.aiType = AIDEFAULT;
        self.angle = 0.0;
        self.isActive = true;
    }
//    NSLog(@"ERROR, DO NOT USE INIT ON SPRITE OBJECT");
    return self;
}

- (void)render {
    GLshort vertices [] = {
        0.0, rect.size.height,
        rect.size.width, rect.size.height,
        0.0, 0.0,
        rect.size.width, 0.0,
    };
    GLfloat textureCoords[ ] = {
        texCoords.origin.x,    texCoords.size.height, //links unten
        texCoords.size.width,  texCoords.size.height, //rechts unten
        texCoords.origin.x,    texCoords.origin.y,    //links oben
        texCoords.size.width,  texCoords.origin.y     //rechts oben
    };
    
//    glColor4f(1, 1, 1, 1);
    glBindTexture(GL_TEXTURE_2D, [[TextureManager getInstance] getTextureID:textureName]);
    glVertexPointer(2, GL_SHORT, 0, vertices);	
    glTexCoordPointer(2, GL_FLOAT, 0, textureCoords);
    
    glPushMatrix();
    glTranslatef(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/2, 0.0);
    glRotatef(angle, 0, 0, 1); //angle = 0 = keine Rotation
    glTranslatef(-rect.size.width/2, -rect.size.height/2, 0.0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();
}

- (void)setType:(int)t {
    //Set Textures
    switch (t) {
        case CAR:
            textureName = @"car_animated.png";
            break;
        case PLAYER:
            textureName = @"car_animated.png";
            break;
        case CUSTOMER:
            textureName = @"NoTexture.png";
            break;
        default:
            break;
    }
}

- (int)getType {
    return type;
}

- (void)update {
    [super update];
    [[AI getInstance] aiTick:self];
}

@end
