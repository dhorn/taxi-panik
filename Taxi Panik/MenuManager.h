//
//  MenuManager.h
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Button.h"
#import "ScrollView.h"
#import "InAppShop.h"

@class GameManager;

enum ELEMENTS {
    BUTTON,
    SCROLLVIEW,
    INAPP,
};

@interface MenuManager : NSObject {
    NSMutableArray* guiElements;
}

//class methods
+ (MenuManager*)getInstance;
- (void)preloader;

//Public
- (void)addElement:(int)element rect:(CGRect)rect angle:(float)angle title:(NSString*)title; 
- (void)touch:(CGPoint)t point2:(CGPoint)p2 event:(int)ev;
- (void)render;
- (void)destroy;

@end
