//
//  CollisionBitmap.h
//  Taxi Panik
//
//  Created by Dominik Horn on 09.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Bit;

@interface CollisionBitmap : NSObject {
    NSMutableDictionary* bits;
    CGSize size;
}

@property (nonatomic, retain) NSMutableDictionary* bits;
@property CGSize size;

- (id)initWithSize:(CGSize)s;

- (void)addBit:(Bit*)bit;
- (Bit*)getBitAt:(CGPoint)pos;
- (NSArray*)getBitsForSpace:(CGRect)space;

@end
