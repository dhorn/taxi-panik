//
//  CustomAVAudioPlayer.m
//  Taxi Panik
//
//  Created by Dominik Horn on 29.10.12.
//
//

#import "CustomAVAudioPlayer.h"

@implementation CustomAVAudioPlayer
@synthesize name = _name;
@synthesize wasPlaying = _wasPlaying;

- (id)init
{
    self = [super init];
    if (self) {
        self.name = @"Uninitialized Name";
        self.wasPlaying = false;
    }
    return self;
}

- (id)initWithContentsOfURL:(NSURL *)url error:(NSError **)outError {
    self = [super initWithContentsOfURL:url error:outError];
    if (self) {
        self.name = [url lastPathComponent];
    }
    return self;
}

@end
