//
//  AI.m
//  Taxi Panik
//
//  Created by Dominik Horn on 11.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AI.h"
#import "Sprite.h"

@implementation AI

+ (AI*)getInstance {
    static AI* ai;
    if (!ai) {
        ai = [[AI alloc] init];
    }
    return ai;
}

- (void)aiTick:(Sprite*)object {
    switch (object.aiType) {
        case AIDEFAULT:
            
            break;
        case AICAR:
            
            break;            
        case AICUSTOMER:
            
            break;
        default:
            break;
    }
}

@end
