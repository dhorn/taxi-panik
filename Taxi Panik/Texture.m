//
//  Texture.m
//  Taxi Panik
//
//  Created by Dominik Horn on 24.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Texture.h"

@implementation Texture
@synthesize textureID;
@synthesize width;
@synthesize height;

@end
