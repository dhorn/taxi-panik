//
//  OPoint.h
//  Taxi Panik
//
//  Created by Dominik Horn on 15.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPoint : NSObject {
    CGPoint pos;
}

+ (OPoint*)pwp:(CGPoint)p;

@property CGPoint pos;

@end
